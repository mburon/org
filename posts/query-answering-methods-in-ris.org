#+TITLE: Query Answering Methods in RDF Integration Systems


An /RDF integration system/ is of the form $S=\langle \onto, \rules, \mappings, \extensions \rangle$, 
where $\onto$ is a first order RDFS ontology (without blank nodes), $\rules$ is the set of RDFS entailment rules 
composed of two disjoint rule sets $\rules_{a}$ and $\rules_{c}$, 
finally $\mappings$ and $\extensions$ are RIS mappings with their extensions. 

The /certain answer set/ of a BGP query $q$ on $S$ is: 
$$\cert(q, S) = \{\phi(\bar x)  \mid (\rdfGraph \cup \onto) \models_{\rules}^{\phi} q(\bar x)\}$$
where $\phi(\bar x)$ is made of values from $\extensions$.

* Classifying Query Answering Methods by Kind of Reasoning

** Two Kinds of Reasoning 

Since $\rules = \rules_{a} \cup \rules_{c}$, we can split the reasoning w.r.t. $\rules$ in two: 
- reasoning w.r.t. $\rules_{a}$, to entail new assertion triples,
- reasoning w.r.t. $\rules_{c}$, to entail new constraint (ontological) triples. 

Moreover, we have proven that the saturation w.r.t. $\rules$ of RDF graph can be obtained by saturating 
w.r.t. $\rules_{a}$ then $\rules_{c}$ or in the inverse order:

#+BEGIN_prop
For $\graph$ an RDF graph whose ontology is FO RDFS ontology, it holds:
$$\graph^{\rules} = \left (\graph^{\rules_{a}} \right )^{\rules_{c}} = \left (\graph^{\rules_{c}} \right )^{\rules_{a}}$$
#+END_prop

** Methods for reasoning w.r.t. $\rules_{a}$

We define methods to handle the reasoning w.r.t. $\rules_{a}$ in $S$ during the computation of the certain answer of a BGPQ $q$:

- *saturate w.r.t. $\rules_{a}$*. This method is only applicable when the graph has been saturated, i.e., in a materialization 
context. It consists of saturating the materialization of the induced graph $\rdfGraph$ with the ontology $\onto$ w.r.t.
$\rules_{a}$ before evaluating the query. This can be done in pre-processing (prior to any query being answered).  
- *reformulate w.r.t. $\rules_{a}$*.  This methods consists of reformulating the query $q$ w.r.t. $\rules_{a}$ and $\onto$ 
as $\mathcal{Q_{a}} = \mathrm{Ref}_{a}(q, \onto)$.
- *saturate mappings*. This method consists of replacing the mappings $\mappings$ with the saturated mappings 
$\mappings^{\rules_{a}, \onto}$. This can be done in pre-processing.

** Methods for reasoning w.r.t. $\rules_{c}$

We define methods to handle the reasoning w.r.t. $\rules_{c}$ in $S$ during the computation of certain answer of a BGPQ $q$:

- *saturate w.r.t. $\rules_{c}$*. This method is only applicable in materialization scenarios. It consists of 
saturating the materialization of the ontology $\onto$ w.r.t. $\rules_{c}$ before evaluating the query. 
This can be done in pre-processing.
- *reformulate w.r.t. $\rules_{c}$*. This method consists of reformulating the query $q$ w.r.t. $\rules_{c}$ 
and $\onto$ as $\mathcal{Q_{c}} = \mathrm{Ref}_{c}(q, \onto)$.
- *mappings for $\onto^{\rules_{c}}$*.  This method consists of adding the mappings $\mappings_{\onto^{\rules_{c}}}$ 
and their extensions $\extensions_{\onto^{\rules_{c}}}$ to $S$ in order to populate 
$\onto^{\rules_{c}}$ (the saturated ontology) in the induced graph. This method can be applied in pre-processing.

** Combinations of methods for reasoning w.r.t. $\rules$

We can combine one method for reasoning w.r.t. $\rules_{a}$ with one other w.r.t. $\rules_{c}$ in order to compute $\cert(q, S)$ certain answers of $q$ on $S$. The following table contains an expression of $\cert(q, S)$ for each combination. Each of this expression will lead to a computation according we choose a materialization or mediator approach.

When we use a *materialization approach* all combinations in the following table are available. And we compute $\cert(q, S)$ by evaluating a UBGPQ on a materialized graph as follow:
- expressions in first line or first column have the form $Q(\graph^{\rules})$. Its computation is done by (i) materializing the graph $\graph$, (ii) saturating it using $\rules$ and (iii) evaluating $Q$ in the saturation. (i) and (ii) can be done in pre-processing.
- other expressions have the form $\cert(Q, \langle \emptyset, \emptyset, \mappings, \extensions \rangle)$. Its computation is done by (i) materializing the induced graph $\graph^{\mappings}_{\extensions}$, (ii) evaluating $Q$ on the materialization. (i) can be done in pre-processing.

When we use a *mediator approach*, combinations of the first line and first column are not available. So, all expressions have the form $\cert(Q, \langle \emptyset, \emptyset, \mappings, \extensions \rangle)$. We compute the certain answers by (i) getting a rewriting of $Q$ using $\mappings$ (ii) evaluating the rewriting on $\extensions$ using the mediator. 

Finally, we have 13 methods for query answering in RIS, 9 methods by materialization and 4 methods by mediation.

|                                     | saturate w.r.t. $\rules_{a}$                                                                                  | reformulate w.r.t. $\rules_{a}$                                                                                                                                  | saturate mappings                                                                                                                                                      |
|-------------------------------------+---------------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| *saturate w.r.t. $\rules_{c}$*      | $q((\rdfGraph \cup \onto)^{\rules_{a} \cup \rules_{c}})$                                                      | $\mathcal{Q_{a}}(\rdfGraph \cup \onto^{\rules_{c}})$                                                                                                             | $q(\graph^{\mappings^{\rules_{a}, \onto}}_{\extensions} \cup \onto^{\rules_{c}})$                                                                                      |
| *reformulate w.r.t. $\rules_{c}$*   | $\mathcal{Q_{c}}((\rdfGraph \cup \onto)^{\rules_{a}})$                                                        | $\cert(\mathcal{Q_{c,a}}, \langle \emptyset, \emptyset, \mappings, \extensions \rangle)$                                                                         | $\cert(\mathcal{Q_{c}}, \langle \emptyset, \emptyset, \mappings^{\rules_{a}, \onto}, \extensions \rangle)$                                                             |
| *mappings for $\onto_{\rules_{c}}$* | $q((\rdfGraph \cup \graph^{\mappings_{\onto^{\rules_{c}}}}_{\extensions_{\onto^{\rules_{c}}}})^{\rules_{a}})$ | $\cert(\mathcal{Q_{a}}, \langle \emptyset, \emptyset, \mappings \cup \mappings_{\onto^{\rules_{c}}}, \extensions \cup \extensions_{\onto^{\rules_{c}}} \rangle)$ | $\cert(q, \langle \emptyset, \emptyset, \mappings^{\rules_{a}, \onto} \cup \mappings_{\onto^{\rules_{c}}}, \extensions \cup \extensions_{\onto^{\rules_{c}}} \rangle)$ |
