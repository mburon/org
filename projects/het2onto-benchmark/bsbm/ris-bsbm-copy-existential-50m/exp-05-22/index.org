#+TITLE: Experiment on Big Relational Datasource

This experiments are made following [[file:../../index.org][this details]].

* Methods Comparison 
:PROPERTIES:
:header-args: :var cols=answer-num-col rewor="REW_OR/stats.csv" ref="REF/stats.csv" matref="MAT_REF/stats.csv" matsat="MAT_SAT/stats.csv"
:END:

** Total time 

It is the total query answering time.

The materialization (MAT_REF) takes ? (?ms) to compute and contains 108 501 139 triples.
The saturated materialization (MAT_SAT) takes 15h06min (54 386 051ms) to compute and contains 184 854 053 triples. 

#+NAME: comparison-files
#+BEGIN_src python :exports none :results none
  files = {
      'MAT': matsat,
      'MAT-CA': matref,
      'REW-CA': ref,
      'REW-C': rewor
  }
#+END_src

#+HEADER: :var col="T_TOTAL"
#+HEADER: :var title="total-time-comparison"
#+BEGIN_SRC python :noweb yes :results file :exports results
import sys
sys.path.append('../../')
from exp import print_total

<<comparison-files>>
return print_total(files, col, title)
#+END_SRC

#+RESULTS:
[[file:total-time-comparison.png]]

** Main Statistics on Queries

#+BEGIN_src python :results table :exports results :noweb yes :tangle test
  import sys
  sys.path.append('../../')
  from exp import load_stats

  df = load_stats(ref, None, timeout=False)
  df = df[['N_TRI', 'N_REF', 'N_ANS']].T.astype(int).reset_index()

  return  [list(df)] + [None] + df.values.tolist()
#+END_src

#+RESULTS:
| index |   Q01 |   Q01a |   Q01b | Q02 | Q02a | Q02b | Q02c |  Q03 |  Q04 | Q07 | Q07a |    Q09 |  Q10 |    Q13 |    Q13a |     Q13b |     Q14 |    Q16 |   Q19 |  Q19a | Q20 | Q20a |  Q20b |  Q20c |   Q21 |  Q22 |  Q22a |     Q23 |
|-------+-------+--------+--------+-----+------+------+------+------+------+-----+------+--------+------+--------+---------+----------+---------+--------+-------+-------+-----+------+-------+-------+-------+------+-------+---------|
| N_TRI |     5 |      5 |      5 |   6 |    6 |    6 |    6 |    5 |    2 |   3 |    3 |      1 |    3 |      4 |       4 |        4 |       3 |      4 |     9 |     9 |  11 |   11 |    11 |    11 |     3 |    4 |     4 |       7 |
| N_REF |    21 |    175 |   1407 |  63 |  147 |  525 | 1225 | 4375 |    1 |   5 |   19 |      7 | 9350 |     84 |    5628 |     5628 |       1 |    201 |   525 |  1225 |  63 |  525 |  1225 |  4221 |  9350 |   40 |   520 |     192 |
| N_ANS | 15514 | 111793 | 863729 | 124 |  598 | 1058 | 1570 |    5 | 4487 |   2 |    3 | 299902 |   10 | 167760 | 4416946 | 10049829 | 3000000 | 249004 | 39826 | 60834 | 904 | 7818 | 10486 | 51988 | 37176 | 1528 | 18588 | 1329887 |


** Rewritings Processing Time

Rewritings processing is the part of query answering that concerns only rewriting building and rewriting optimization. It doesn't includes the query plan optimization and evaluation.

#+NAME: group-display
#+HEADER: :var group=ref-rew-col
#+HEADER: :var title="rewriting-processing-time"
#+BEGIN_SRC python :exports results :results file
  import pandas as pd
  import matplotlib.pyplot as plt
  import numpy as np
  import re

  dfs = []
  group[0].append('INPUT')
  group[0].append('T_TOTAL')
  col_name = 'GROUP_SUM'

  row_to_drop = ['Q02old', 'Q02d', 'Q02e', 'Q06a', 'Q06b', 'Q10a', 'Q10b', 'Q11', 'Q19b', 'Q19c']

  def load(f, name):
      df = pd.read_csv(f, sep='\t')[group[0]]
      df = df.groupby(['INPUT']).mean()
      to_drop = set(row_to_drop)
      to_drop &= set(df.index)
      df = df.drop(to_drop)
      is_timeout = df['T_TOTAL'] < 600000 # 10min
      df = df[is_timeout]
      df = df.drop(columns=['T_TOTAL'])
      df[col_name] = df.sum(axis=1)
      df = df.rename(index=str, columns={col_name: name})
      dfs.append(df.loc[:, name])

  # ref
  load(ref, 'REW-CA')
  # rew
  load(rewor, 'REW-C')

  data = pd.concat(dfs, axis=1)

  plt.figure(figsize=(15,4));
  ax = data.plot.bar(figsize=(15,6), bottom=1 , width=0.8);
  plt.title(title)

  if(group[0][0][0] == 'T'):
      plt.ylabel('time (ms)')
      ax.set_yscale('log')

  file_name =  title + '.png'
  plt.savefig(file_name)
  return file_name
#+END_SRC

#+RESULTS: group-display
[[file:rewriting-processing-time.png]]

** Query Plan Processing Time

Query plan processing time are the sum of query plan optimization and query plan evaluation. Since the two method returns minimal equivalent rewriting set, we expect that there is no time different between the two method at this step.  

#+HEADER: :var group=eval-col
#+HEADER: :var title="query-plan-time"
#+BEGIN_SRC python :exports results :results file :noweb yes
<<group-display>>
#+END_SRC

#+RESULTS:
[[file:query-plan-time.png]]

* Reformulation Based Method (~REW-CA~)
:PROPERTIES:
:header-args: :var file="REF/stats.csv"
:END:
** Raw Statistics

#+NAME: raw-mean
#+BEGIN_src python :results table :exports results
  import pandas as pd
  import matplotlib.pyplot as plt
  import numpy as np

  row_to_drop = ['Q02old', 'Q02d', 'Q02e', 'Q06a', 'Q06b', 'Q10a', 'Q10b', 'Q11', 'Q19b', 'Q19c']
  f = file.strip()
  df = pd.read_csv(f , sep='\t');

  group = df.groupby(['INPUT']).mean()

  group = df.groupby(['INPUT']).mean()
  is_timeout = group['T_TOTAL'] < 600000 # 10min
  group = group[is_timeout]
  to_drop = set(row_to_drop)
  to_drop &= set(group.index)
  group = group.drop(list(to_drop)).reset_index()
  
  return  [list(group)] + [None] + group.values.tolist()
#+END_src

#+RESULTS: raw-mean
| INPUT |      N_ANS | N_TRI |  N_REF |   N_REW | N_CLASH | N_COVER | T_REF |    T_REW | T_CLASH | T_COVER | T_CORE |    T_OP |    T_QEV |  T_TOTAL |
|-------+------------+-------+--------+---------+---------+---------+-------+----------+---------+---------+--------+---------+----------+----------|
| Q01   |    15514.0 |   5.0 |   21.0 |    16.0 |    12.0 |     4.0 |   0.4 |   1209.0 |     0.2 |     1.4 |   25.8 |     4.2 |    140.8 |   1381.8 |
| Q01a  |   111793.0 |   5.0 |  175.0 |   128.0 |    96.0 |    32.0 |   2.6 |   9199.4 |     0.6 |     8.2 |  172.8 |    21.8 |   1078.0 |  10483.4 |
| Q01b  |   863729.0 |   5.0 | 1407.0 |  1024.0 |   768.0 |   256.0 |  17.8 |  72170.4 |     7.2 |   138.0 | 1413.6 |   196.0 |   8110.8 |  82053.8 |
| Q02   |      124.0 |   6.0 |   63.0 |    32.0 |    24.0 |     8.0 |   1.4 |  15424.0 |     0.2 |     2.6 |   42.0 |     7.4 |    122.2 |  15599.8 |
| Q02a  |      598.0 |   6.0 |  147.0 |    64.0 |    48.0 |    16.0 |   3.0 |  36698.6 |     0.8 |     3.4 |   84.8 |    14.2 |    243.8 |  37048.6 |
| Q04   |     4487.0 |   2.0 |    1.0 |     8.0 |     6.0 |     2.0 |   0.2 |     29.4 |     0.0 |     0.0 |    1.6 |     0.2 |     27.0 |     58.4 |
| Q05   |  3000000.0 |   4.0 |    1.0 |     8.0 |     7.0 |     1.0 |   0.2 |     28.4 |     0.0 |     0.0 |    3.2 |     0.6 |  15661.2 |  15693.6 |
| Q05a  |  1793489.0 |   4.0 |    3.0 |     2.0 |     nan |     2.0 |   0.0 |    370.2 |     0.0 |     0.0 |    1.6 |     1.6 |   8570.6 |   8944.0 |
| Q05b  |  2454945.0 |   4.0 |    7.0 |     4.0 |     nan |     4.0 |   0.4 |    839.2 |     0.2 |     0.0 |    3.2 |     2.8 |  12546.0 |  13391.8 |
| Q06   |  2790902.0 |   3.0 |    5.0 |     2.0 |     nan |     2.0 |   0.0 |    318.2 |     0.2 |     0.6 |    4.0 |     1.4 |  13688.8 |  14013.2 |
| Q07   |        2.0 |   3.0 |    5.0 |  2562.0 |     nan |  2562.0 |   0.0 |    583.6 |     3.4 |  2641.2 |  229.6 |  3237.8 |  29872.0 |  36567.6 |
| Q07a  |        3.0 |   3.0 |   19.0 | 10248.0 |     nan |  5124.0 |   0.4 |   2187.4 |    12.4 | 19947.0 |  413.0 | 10564.6 |  63509.6 |  96634.4 |
| Q08   |  3000000.0 |   3.0 |    1.0 |     1.0 |     nan |     1.0 |   0.0 |     40.2 |     0.0 |     0.0 |    4.8 |     0.6 |  19342.8 |  19388.4 |
| Q09   |   299902.0 |   1.0 |    7.0 |     2.0 |     nan |     2.0 |   0.2 |    200.2 |     0.0 |     0.0 |    3.2 |     0.8 |    813.2 |   1017.6 |
| Q12   |    14571.0 |   5.0 |    2.0 |     8.0 |     7.0 |     1.0 |   0.4 |    310.8 |     0.0 |     0.0 |    2.0 |     0.8 |    122.6 |    436.6 |
| Q12a  |    31322.0 |   5.0 |    6.0 |    16.0 |    14.0 |     2.0 |   0.2 |    857.0 |     0.0 |     0.0 |    3.2 |     1.0 |    370.0 |   1231.4 |
| Q12b  |   117426.0 |   5.0 |   14.0 |    32.0 |    28.0 |     4.0 |   0.4 |   1986.6 |     0.2 |     0.0 |    6.6 |     2.0 |   1108.2 |   3104.0 |
| Q13   |   167760.0 |   4.0 |   84.0 |    50.0 |     nan |    42.0 |   1.0 |   5809.0 |     0.2 |    34.4 |   78.6 |    30.0 |   2850.8 |   8804.0 |
| Q13a  |  4416946.0 |   4.0 | 5628.0 |  3200.0 |     nan |  2688.0 |  47.8 | 372855.2 |    29.6 |  7608.2 | 5308.0 |  5363.0 | 111480.8 | 502692.6 |
| Q13b  | 10049829.0 |   4.0 | 5628.0 |  3200.0 |     nan |  2688.0 |  52.8 | 375818.8 |    34.8 |  7696.6 | 5406.2 |  5425.4 | 181357.0 | 575791.6 |
| Q14   |  3000000.0 |   3.0 |    1.0 |     1.0 |     nan |     1.0 |   0.0 |    141.6 |     0.0 |     0.0 |    4.2 |     1.0 |  13080.2 |  13227.0 |
| Q15   |  1500000.0 |   5.0 |    1.0 |     4.0 |     3.0 |     1.0 |   0.2 |    107.0 |     0.0 |     0.0 |    4.2 |     1.4 |  13286.8 |  13399.6 |
| Q16   |   249004.0 |   4.0 |  201.0 |   128.0 |     nan |   128.0 |   2.0 |  15070.2 |     1.0 |    18.8 |  457.2 |    49.4 |   7287.4 |  22886.0 |
| Q17   |  2790902.0 |   2.0 |    5.0 |     2.0 |     nan |     2.0 |   0.2 |    262.8 |     0.0 |     0.4 |    3.2 |     1.0 |  10663.8 |  10931.4 |
| Q18   |  3000000.0 |   4.0 |    1.0 |     4.0 |     3.0 |     1.0 |   0.2 |     91.4 |     0.0 |     0.0 |    6.4 |     1.4 |  21984.0 |  22083.4 |
| Q19   |    39826.0 |   9.0 |  525.0 |   448.0 |     nan |   128.0 |  15.0 | 256995.2 |     3.0 |   896.8 |  887.0 |   134.6 |   3026.4 | 261958.0 |
| Q20   |      904.0 |  11.0 |   63.0 |     8.0 |     nan |     8.0 |   2.0 |  41787.2 |     0.2 |    95.6 |   81.8 |    17.4 |   1977.8 |  43962.0 |
| Q20a  |     7818.0 |  11.0 |  525.0 |    64.0 |     nan |    64.0 |  17.0 | 342123.2 |     3.8 |   776.0 |  610.4 |   126.0 |  16884.8 | 360541.2 |
| Q21   |    37176.0 |   3.0 | 9350.0 | 14180.0 |  6486.0 |  6401.0 | 114.2 | 109224.8 |    58.0 | 27936.8 |  852.4 | 17280.0 | 145699.2 | 301165.4 |
| Q22   |     1528.0 |   4.0 |   40.0 |    32.0 |     nan |    32.0 |   1.0 |    637.2 |     0.4 |     1.6 |    4.4 |     7.0 |    860.4 |   1512.0 |
| Q22a  |    18588.0 |   4.0 |  520.0 |   384.0 |     nan |   384.0 |   6.2 |   6874.8 |     4.6 |   148.6 |   50.6 |   143.2 |  10300.6 |  17528.6 |
| Q23   |  1329887.0 |   7.0 |  192.0 |    80.0 |    56.0 |    24.0 |   3.8 |  30196.2 |     1.4 |     8.8 |   33.2 |    14.2 |  18362.2 |  48619.8 |
| Q23a  |  2008985.0 |   7.0 |  448.0 |   160.0 |   112.0 |    48.0 |   6.2 |  69899.8 |     1.8 |    21.0 |   66.0 |    25.2 |  33587.6 | 103607.6 |

** Rewriting Numbers
#+HEADER: :var title="rewritings-numbers"
#+HEADER: :var cols=rew-num-col
#+BEGIN_SRC python :exports results :results file :noweb yes
<<display_cols>>
#+END_SRC

#+RESULTS:
[[file:rewritings-numbers.png]]

** Rewriting Processing Times

#+HEADER: :var title="times-for-rewriting-ref"
#+HEADER: :var cols=rew-col
#+BEGIN_SRC python :exports results :results file :noweb yes
<<display_cols>>
#+END_SRC

#+RESULTS:
[[file:times-for-rewriting-ref.png]]

** Query Plan Processing Times

#+HEADER: :var title="times-for-plan-evaluation-ref"
#+HEADER: :var cols=eval-col
#+BEGIN_SRC python :exports results :results file :noweb yes
<<display_cols>>
#+END_SRC

#+RESULTS:
[[file:times-for-plan-evaluation-ref.png]]

* Rewriting Based Method (~REW-C~)
:PROPERTIES:
:header-args: :var file="REW_OR/stats.csv"
:END:

** Raw Statistics

#+BEGIN_SRC python :exports results :results table :noweb yes
<<raw-mean>>
#+END_SRC

#+RESULTS:
| INPUT |      N_ANS | N_TRI | N_REF |   N_REW | N_CLASH | N_COVER | T_REF |   T_REW | T_CLASH | T_COVER |  T_CORE |    T_OP |    T_QEV |  T_TOTAL |
|-------+------------+-------+-------+---------+---------+---------+-------+---------+---------+---------+---------+---------+----------+----------|
| Q01   |    15514.0 |   5.0 |   1.0 |    16.0 |    12.0 |     4.0 |   0.0 |   458.4 |     0.0 |     0.8 |    24.2 |     3.8 |    142.0 |    629.2 |
| Q01a  |   111793.0 |   5.0 |   1.0 |   128.0 |    96.0 |    32.0 |   0.0 |   478.0 |     0.2 |     5.4 |   195.6 |    23.0 |   1049.2 |   1751.4 |
| Q01b  |   863729.0 |   5.0 |   1.0 |  1024.0 |   768.0 |   256.0 |   0.0 |   503.2 |     1.2 |    76.6 |  1583.0 |   213.6 |   8236.0 |  10613.6 |
| Q02   |      124.0 |   6.0 |   1.0 |    32.0 |    24.0 |     8.0 |   0.0 |   650.4 |     0.2 |     1.4 |    45.2 |     7.2 |    122.8 |    827.2 |
| Q02a  |      598.0 |   6.0 |   1.0 |    64.0 |    48.0 |    16.0 |   0.0 |   645.2 |     0.0 |     3.0 |   178.6 |    13.0 |    243.8 |   1083.6 |
| Q02b  |     1058.0 |   6.0 |   1.0 |   256.0 |   192.0 |    64.0 |   0.0 |   660.6 |     0.2 |    13.0 |   377.8 |    56.6 |    867.6 |   1975.8 |
| Q02c  |     1570.0 |   6.0 |   1.0 |   512.0 |   384.0 |   128.0 |   0.0 |   670.4 |     0.8 |    30.2 |   739.8 |   113.2 |   1754.8 |   3309.2 |
| Q03   |        5.0 |   5.0 |   1.0 |  1024.0 |     nan |   512.0 |   0.2 |   521.8 |     2.2 |   357.0 |  9113.2 |   801.2 |   6769.6 |  17565.2 |
| Q04   |     4487.0 |   2.0 |   1.0 |     8.0 |     6.0 |     2.0 |   0.0 |   129.8 |     0.0 |     0.0 |     1.2 |     0.4 |     27.0 |    158.4 |
| Q05   |  3000000.0 |   4.0 |   1.0 |     8.0 |     7.0 |     1.0 |   0.2 |   128.4 |     0.0 |     0.0 |     3.4 |     0.6 |  15917.0 |  16049.6 |
| Q05a  |  1793489.0 |   4.0 |   1.0 |     2.0 |     nan |     2.0 |   0.4 |   381.8 |     0.0 |     0.0 |     1.8 |     1.8 |   8807.0 |   9192.8 |
| Q05b  |  2454945.0 |   4.0 |   1.0 |     4.0 |     nan |     4.0 |   0.0 |   374.6 |     0.0 |     0.0 |     3.2 |     2.4 |  12710.6 |  13090.8 |
| Q06   |  2790902.0 |   3.0 |   1.0 |     2.0 |     nan |     2.0 |   0.2 |   244.2 |     0.0 |     0.2 |     5.0 |     1.4 |  13699.2 |  13950.2 |
| Q07   |        2.0 |   3.0 |   1.0 |  2562.0 |     nan |  2562.0 |   0.0 |   493.0 |     3.6 |  2819.2 |   231.6 |  3517.0 |  29920.8 |  36985.2 |
| Q07a  |        3.0 |   3.0 |   1.0 |  5124.0 |     nan |  5124.0 |   0.0 |   465.4 |     6.0 | 12426.6 |   483.2 | 14037.6 |  63556.4 |  90975.2 |
| Q08   |  3000000.0 |   3.0 |   1.0 |     1.0 |     nan |     1.0 |   0.0 |   142.2 |     0.0 |     0.0 |     5.0 |     0.8 |  21213.2 |  21361.2 |
| Q09   |   299902.0 |   1.0 |   1.0 |     2.0 |     nan |     2.0 |   0.4 |   241.4 |     0.0 |     0.0 |     4.4 |     1.0 |    831.2 |   1078.4 |
| Q10   |       10.0 |   3.0 |   1.0 |  7690.0 |     nan |  7690.0 |   0.0 |  1094.0 |    14.0 | 28513.0 |   799.4 | 28894.8 | 153027.4 | 212342.6 |
| Q12   |    14571.0 |   5.0 |   2.0 |     8.0 |     7.0 |     1.0 | 384.8 |   749.2 |     0.0 |     0.0 |     2.2 |     0.4 |    120.6 |   1257.2 |
| Q12a  |    31322.0 |   5.0 |   2.0 |    16.0 |    14.0 |     2.0 | 351.2 |   698.6 |     0.0 |     0.0 |     3.6 |     1.0 |    364.2 |   1418.6 |
| Q12b  |   117426.0 |   5.0 |   2.0 |    32.0 |    28.0 |     4.0 | 349.8 |   717.2 |     0.0 |     0.0 |     7.0 |     1.8 |   1070.4 |   2146.2 |
| Q13   |   167760.0 |   4.0 |  16.0 |    50.0 |     nan |    42.0 |   3.4 |  3310.4 |     0.0 |    34.2 |    95.8 |    37.0 |   2743.2 |   6224.0 |
| Q13a  |  4416946.0 |   4.0 |  16.0 |  3200.0 |     nan |  2688.0 |   3.4 |  3404.0 |     5.2 |  5629.4 |  5479.0 |  6106.8 | 112568.6 | 133196.4 |
| Q13b  | 10049829.0 |   4.0 |  16.0 |  3200.0 |     nan |  2688.0 |   3.6 |  3424.8 |     4.0 |  5613.6 |  5690.0 |  6415.8 | 183236.2 | 204388.0 |
| Q14   |  3000000.0 |   3.0 |   1.0 |     1.0 |     nan |     1.0 |   0.2 |   345.2 |     0.0 |     0.0 |     4.4 |     1.6 |  13111.6 |  13463.0 |
| Q15   |  1500000.0 |   5.0 |   1.0 |     4.0 |     3.0 |     1.0 |   0.0 |   280.4 |     0.0 |     0.0 |     3.6 |     1.6 |  13661.6 |  13947.2 |
| Q16   |   249004.0 |   4.0 |   1.0 |   128.0 |     nan |   128.0 |   0.2 |   285.8 |     0.2 |    10.4 |   457.2 |    49.8 |   7446.2 |   8249.8 |
| Q17   |  2790902.0 |   2.0 |   1.0 |     2.0 |     nan |     2.0 |   0.0 |   208.8 |     0.0 |     0.2 |     4.0 |     1.4 |  10798.4 |  11012.8 |
| Q18   |  3000000.0 |   4.0 |   1.0 |     4.0 |     3.0 |     1.0 |   0.2 |   271.4 |     0.0 |     0.0 |     7.4 |     1.4 |  22916.8 |  23197.2 |
| Q19   |    39826.0 |   9.0 |   1.0 |   448.0 |     nan |   128.0 |   0.2 |  1129.0 |     1.4 |  1248.4 |  1094.2 |   158.2 |   3051.4 |   6682.8 |
| Q19a  |    60834.0 |   9.0 |   1.0 |   896.0 |     nan |   256.0 |   0.2 |  1196.2 |     2.8 |  2558.8 |  2168.4 |   312.4 |   5861.8 |  12100.6 |
| Q20   |      904.0 |  11.0 |   1.0 |     8.0 |     nan |     8.0 |   0.0 |  1690.8 |     0.0 |    93.4 |    81.2 |    18.4 |   2177.4 |   4061.2 |
| Q20a  |     7818.0 |  11.0 |   1.0 |    64.0 |     nan |    64.0 |   0.0 |  1410.4 |     0.4 |   822.8 |   799.2 |   161.0 |  17337.4 |  20531.2 |
| Q20b  |    10486.0 |  11.0 |   1.0 |   128.0 |     nan |   128.0 |   0.6 |  1467.0 |     0.8 |  1775.2 |  1368.8 |   310.2 |  31928.6 |  36851.2 |
| Q20c  |    51988.0 |  11.0 |   1.0 |   512.0 |     nan |   512.0 |   0.2 |  1692.2 |     3.0 |  6768.4 |  6232.8 |  1323.4 | 123776.8 | 139796.8 |
| Q20d  |   201747.0 |  11.0 |   1.0 |  1024.0 |     nan |  1024.0 |   0.0 |  1695.6 |     6.4 | 12742.4 | 11809.8 |  3181.4 | 312638.0 | 342073.6 |
| Q21   |    37176.0 |   3.0 |   1.0 | 12865.0 |  6459.0 |  6401.0 |   0.4 |   833.0 |    20.6 | 19377.0 |   805.8 | 19934.8 | 145191.4 | 186163.0 |
| Q22   |     1528.0 |   4.0 |  24.0 |    32.0 |     nan |    32.0 |   0.2 |  1818.0 |     0.4 |     2.4 |     4.4 |     6.8 |    858.0 |   2690.2 |
| Q22a  |    18588.0 |   4.0 | 200.0 |   384.0 |     nan |   384.0 |   2.8 | 15664.8 |     3.2 |   143.0 |    61.4 |   172.0 |  10246.4 |  26293.6 |
| Q23   |  1329887.0 |   7.0 |  64.0 |    80.0 |    56.0 |    24.0 | 810.8 | 23670.6 |     1.2 |    10.4 |    36.8 |    12.8 |  19066.6 |  43609.2 |
| Q23a  |  2008985.0 |   7.0 |  64.0 |   160.0 |   112.0 |    48.0 | 712.4 | 23375.0 |     1.0 |    19.8 |    74.6 |    26.6 |  34392.8 |  58602.2 |

** Rewriting Numbers
#+HEADER: :var title="rewritings-numbers"
#+HEADER: :var cols=rew-num-col
#+BEGIN_SRC python :exports results :results file :noweb yes
<<display_cols>>
#+END_SRC

#+RESULTS:
[[file:rewritings-numbers.png]]

** Rewriting Processing Times

#+HEADER: :var title="times-for-rewriting-rew"
#+HEADER: :var cols=rew-col
#+BEGIN_SRC python :exports results :results file :noweb yes
<<display_cols>>
#+END_SRC

#+RESULTS:
[[file:times-for-rewriting-rew.png]]


** Query Plan Processing Times

#+HEADER: :var title="times-for-plan-evaluation-rew"
#+HEADER: :var cols=eval-col
#+BEGIN_SRC python :exports results :results file :noweb yes
<<display_cols>>
#+END_SRC

#+RESULTS:
[[file:times-for-plan-evaluation-rew.png]]


* Queries
:PROPERTIES:
:header-args: :var cols=answer-num-col file="REW_OR/stats.csv"
:END:

Queries are build by groups of Q, Qa, Qb ... where Qb is always more general than Qa according to the ontology.

#+BEGIN_src shell :exports none :results none
cp ../queries.txt queries-display.txt
sed -i 's/http:\/\/www.w3.org\/2000\/01\/rdf-schema#/rdfs:/g' queries-display.txt
sed -i 's/http:\/\/www.w3.org\/1999\/02\/22-rdf-syntax-ns#/rdf:/g' queries-display.txt
sed -i 's/http:\/\/www4.wiwiss.fu-berlin.de\/bizer\/bsbm\/v01\/vocabulary\//bsbm:/g' queries-display.txt
sed -i 's/http:\/\/www4.wiwiss.fu-berlin.de\/bizer\/bsbm\/v01\/instances\//bsbm-int:/g' queries-display.txt
sed -i 's/http:\/\/xmlns.com\/foaf\/0.1\//foaf:/g' queries-display.txt
#+END_src

#+include: queries-display.txt example

#+NAME: display_cols
#+HEADER: :var title="number of answers"
#+BEGIN_SRC python :results file :exports results
  import pandas as pd
  import matplotlib.pyplot as plt
  import numpy as np

  row_to_drop = ['Q02old', 'Q02d', 'Q02e', 'Q06a', 'Q06b', 'Q10a', 'Q10b', 'Q11', 'Q19b', 'Q19c']

  f = file.strip()
  df = pd.read_csv(f , sep='\t');
  group = df.groupby(['INPUT']).mean()
  is_timeout = group['T_TOTAL'] < 600000 # 10min
  group = group[is_timeout]
  to_drop = set(row_to_drop)
  to_drop &= set(group.index)
  group = group.drop(list(to_drop))

  data = group.loc[:,cols[0]]

  plt.figure(figsize=(15,4));
  ax = data.plot.bar(figsize=(15,6), bottom=1 , width=0.8);
  plt.title(title)
  if(cols[0][0][0] == 'T'):
      plt.ylabel('time (ms)')
  ax.set_yscale('log')

  try: label_col
  except NameError: label_col = None

  if(label_col != None):
      rects = ax.patches

      labels = group[label_col].astype(int)
    
      for rect, label in zip(rects, labels):
          height = rect.get_height()
          ax.text(rect.get_x() + 5*rect.get_width() / 2, 2, label,
                  ha='center', va='top')

  file_name = title + '.png'
  plt.savefig(file_name)
  return file_name
#+END_SRC

#+RESULTS: display_cols
[[file:number of answers.png]]

** REW Method Query Plans

#+HEADER: :var experiment="REW_OR"
#+BEGIN_SRC shell :results raw :exports results
  for name in `ls -1 $experiment/query-plan/Q*.svg | sort` 
  do
      echo - `echo $name | grep -Po "Q[0-9a-z]+"` 
      echo "[[file:$name][file:$name]]"
  done 
#+END_SRC

#+RESULTS:
- Q01a
[[file:REW_OR/query-plan/Q01a.svg][file:REW_OR/query-plan/Q01a.svg]]
- Q01b
[[file:REW_OR/query-plan/Q01b.svg][file:REW_OR/query-plan/Q01b.svg]]
- Q01
[[file:REW_OR/query-plan/Q01.svg][file:REW_OR/query-plan/Q01.svg]]
- Q02a
[[file:REW_OR/query-plan/Q02a.svg][file:REW_OR/query-plan/Q02a.svg]]
- Q02b
[[file:REW_OR/query-plan/Q02b.svg][file:REW_OR/query-plan/Q02b.svg]]
- Q02c
[[file:REW_OR/query-plan/Q02c.svg][file:REW_OR/query-plan/Q02c.svg]]
- Q02d
[[file:REW_OR/query-plan/Q02d.svg][file:REW_OR/query-plan/Q02d.svg]]
- Q02e
[[file:REW_OR/query-plan/Q02e.svg][file:REW_OR/query-plan/Q02e.svg]]
- Q02old
[[file:REW_OR/query-plan/Q02old.svg][file:REW_OR/query-plan/Q02old.svg]]
- Q02
[[file:REW_OR/query-plan/Q02.svg][file:REW_OR/query-plan/Q02.svg]]
- Q03
[[file:REW_OR/query-plan/Q03.svg][file:REW_OR/query-plan/Q03.svg]]
- Q04
[[file:REW_OR/query-plan/Q04.svg][file:REW_OR/query-plan/Q04.svg]]
- Q05a
[[file:REW_OR/query-plan/Q05a.svg][file:REW_OR/query-plan/Q05a.svg]]
- Q05b
[[file:REW_OR/query-plan/Q05b.svg][file:REW_OR/query-plan/Q05b.svg]]
- Q05
[[file:REW_OR/query-plan/Q05.svg][file:REW_OR/query-plan/Q05.svg]]
- Q06a
[[file:REW_OR/query-plan/Q06a.svg][file:REW_OR/query-plan/Q06a.svg]]
- Q06b
[[file:REW_OR/query-plan/Q06b.svg][file:REW_OR/query-plan/Q06b.svg]]
- Q06
[[file:REW_OR/query-plan/Q06.svg][file:REW_OR/query-plan/Q06.svg]]
- Q07a
[[file:REW_OR/query-plan/Q07a.svg][file:REW_OR/query-plan/Q07a.svg]]
- Q07
[[file:REW_OR/query-plan/Q07.svg][file:REW_OR/query-plan/Q07.svg]]
- Q08
[[file:REW_OR/query-plan/Q08.svg][file:REW_OR/query-plan/Q08.svg]]
- Q09
[[file:REW_OR/query-plan/Q09.svg][file:REW_OR/query-plan/Q09.svg]]
- Q11
[[file:REW_OR/query-plan/Q11.svg][file:REW_OR/query-plan/Q11.svg]]
- Q12a
[[file:REW_OR/query-plan/Q12a.svg][file:REW_OR/query-plan/Q12a.svg]]
- Q12b
[[file:REW_OR/query-plan/Q12b.svg][file:REW_OR/query-plan/Q12b.svg]]
- Q12
[[file:REW_OR/query-plan/Q12.svg][file:REW_OR/query-plan/Q12.svg]]
- Q13
[[file:REW_OR/query-plan/Q13.svg][file:REW_OR/query-plan/Q13.svg]]
- Q14
[[file:REW_OR/query-plan/Q14.svg][file:REW_OR/query-plan/Q14.svg]]
- Q15
[[file:REW_OR/query-plan/Q15.svg][file:REW_OR/query-plan/Q15.svg]]
- Q16
[[file:REW_OR/query-plan/Q16.svg][file:REW_OR/query-plan/Q16.svg]]
- Q17
[[file:REW_OR/query-plan/Q17.svg][file:REW_OR/query-plan/Q17.svg]]
- Q18
[[file:REW_OR/query-plan/Q18.svg][file:REW_OR/query-plan/Q18.svg]]
- Q19a
[[file:REW_OR/query-plan/Q19a.svg][file:REW_OR/query-plan/Q19a.svg]]
- Q19b
[[file:REW_OR/query-plan/Q19b.svg][file:REW_OR/query-plan/Q19b.svg]]
- Q19c
[[file:REW_OR/query-plan/Q19c.svg][file:REW_OR/query-plan/Q19c.svg]]
- Q19
[[file:REW_OR/query-plan/Q19.svg][file:REW_OR/query-plan/Q19.svg]]
- Q20a
[[file:REW_OR/query-plan/Q20a.svg][file:REW_OR/query-plan/Q20a.svg]]
- Q20b
[[file:REW_OR/query-plan/Q20b.svg][file:REW_OR/query-plan/Q20b.svg]]
- Q20c
[[file:REW_OR/query-plan/Q20c.svg][file:REW_OR/query-plan/Q20c.svg]]
- Q20d
[[file:REW_OR/query-plan/Q20d.svg][file:REW_OR/query-plan/Q20d.svg]]
- Q20
[[file:REW_OR/query-plan/Q20.svg][file:REW_OR/query-plan/Q20.svg]]
- Q22a
[[file:REW_OR/query-plan/Q22a.svg][file:REW_OR/query-plan/Q22a.svg]]
- Q22
[[file:REW_OR/query-plan/Q22.svg][file:REW_OR/query-plan/Q22.svg]]
- Q23a
[[file:REW_OR/query-plan/Q23a.svg][file:REW_OR/query-plan/Q23a.svg]]
- Q23
[[file:REW_OR/query-plan/Q23.svg][file:REW_OR/query-plan/Q23.svg]]

* Inputs                                                           :noexport:
#+tblname: ref-rew-col
| T_REF | T_REW | T_CLASH | T_COVER | T_CORE|

#+tblname: rew-col
| T_REF | T_REW | T_CLASH | T_COVER | T_CORE|


#+tblname: eval-col
| T_OP | T_QEV |

#+tblname: ref-num-col
| N_REF | N_REW | N_COVER |

#+tblname: rew-num-col
| N_REW | N_COVER |

#+tblname: answer-num-col
| N_ANS |



