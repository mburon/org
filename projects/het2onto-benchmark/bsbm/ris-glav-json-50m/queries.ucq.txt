Q01<$label, $featureLabel, $value> :-
              triple($product, <http://www.w3.org/2000/01/rdf-schema#label>, $label),
              triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType255>),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>, $productFeature),
              triple($productFeature, <http://www.w3.org/2000/01/rdf-schema#label>, $featureLabel),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value);

Q01a<$label, $featureLabel, $value> :-
              triple($product, <http://www.w3.org/2000/01/rdf-schema#label>, $label),
              triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType32>),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>, $productFeature),
              triple($productFeature, <http://www.w3.org/2000/01/rdf-schema#label>, $featureLabel),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value);

Q01b<$label, $featureLabel, $value> :-
              triple($product, <http://www.w3.org/2000/01/rdf-schema#label>, $label),
              triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType4>),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>, $productFeature),
              triple($productFeature, <http://www.w3.org/2000/01/rdf-schema#label>, $featureLabel),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value);

Q02<$label, $value, $country> :-
              triple($product, <http://www.w3.org/2000/01/rdf-schema#label>, $label),
              triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType255>),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
              triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
              triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType12>);

Q02<$label, $value, $country> :-
              triple($product, <http://www.w3.org/2000/01/rdf-schema#label>, $label),
              triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType255>),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
              triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
              triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType12>);

Q02old<$label, $value, $country> :-
              triple($product, <http://www.w3.org/2000/01/rdf-schema#label>, $label),
              triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType255>),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
              triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
              triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType12>);

Q02a<$label, $value, $country> :-
              triple($product, <http://www.w3.org/2000/01/rdf-schema#label>, $label),
              triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType255>),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
              triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
              triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType1>);

Q02b<$label, $value, $country> :-
              triple($product, <http://www.w3.org/2000/01/rdf-schema#label>, $label),
              triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType32>),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
              triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
              triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType21>);

Q02c<$label, $value, $country> :-
              triple($product, <http://www.w3.org/2000/01/rdf-schema#label>, $label),
              triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType32>),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
              triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
              triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType2>);

Q02d<$label, $value, $country> :-
              triple($product, <http://www.w3.org/2000/01/rdf-schema#label>, $label),
              triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType4>),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value),
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
              triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
              triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType12>);

# Q02e<$label, $value, $country> :-
#               triple($product, <http://www.w3.org/2000/01/rdf-schema#label>, $label),
#               triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType4>),
#               triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value),
#               triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
#               triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
#               triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType1>);

Q03<$product1, $product2> :-
           triple($product1, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType32>),
           triple($product1, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, "1265"),
           triple($product1, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
           triple($product2, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType26>),
           triple($product2, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer);

Q04<$vendor, $vendorLabel, $vendorHomepage> :-
             triple($vendor, <http://www.w3.org/2000/01/rdf-schema#label>, $vendorLabel),
             triple($vendor, <http://xmlns.com/foaf/0.1/homepage>, $vendorHomepage);

Q05<$offer, $offerURL, $vendor, $vendorLabel, $vendorHomepage> :-
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/vendor>, $vendor),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/offerWebpage>, $offerURL),
             triple($vendor, <http://www.w3.org/2000/01/rdf-schema#label>, $vendorLabel),
             triple($vendor, <http://xmlns.com/foaf/0.1/homepage>, $vendorHomepage);

Q05a<$offer, $offerURL> :-
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/vendor>, $vendor),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/offerWebpage>, $offerURL),
             triple($vendor, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
             triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType11>);

Q05b<$offer, $offerURL> :-
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/vendor>, $vendor),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/offerWebpage>, $offerURL),
             triple($vendor, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
             triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType1>);

# add condition on reviewer country and/or on product type and/or producer country
Q06<$review, $product, $rating> :-
             triple($review, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/reviewFor>, $product),
             triple($review, <http://purl.org/stuff/rev#reviewer>, $reviewer),
             triple($review, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/rating>, $rating);

Q06a<$review, $product, $rating> :-
             triple($review, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/reviewFor>, $product),
             triple($review, <http://purl.org/stuff/rev#reviewer>, $reviewer),
             triple($review, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/rating>, $rating),
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
             triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
             triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType11>);

Q06b<$review, $product, $rating> :-
             triple($review, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/reviewFor>, $product),
             triple($review, <http://purl.org/stuff/rev#reviewer>, $reviewer),
             triple($review, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/rating>, $rating),
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
             triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
             triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType2>);

Q07<$country> :-
            triple($org, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://xmlns.com/foaf/0.1/Organization>),
            triple($org, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
            triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType222>);

Q07a<$country> :-
            triple($org, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://xmlns.com/foaf/0.1/Organization>),
            triple($org, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
            triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType22>);

Q08<$offer, $offerURL, $price, $deliveryDays> :-
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/offerWebpage>, $offerURL),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/price>, $price),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/deliveryDays>, $deliveryDays);

Q09<$product, $value> :-
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value);

# Q10<$country, $agentType> :-
#             triple($agent, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, $agentType),
#             triple($agent, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
#             triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType222>);

# Q10a<$country, $agentType> :-
#             triple($agent, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, $agentType),
#             triple($agent, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
#             triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType22>);

# Q10b<$country, $agentType> :-
#             triple($agent, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, $agentType),
#             triple($agent, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
#             triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType2>);

# Q11<$label, $p, $country> :-
#             triple($x, <http://www.w3.org/2000/01/rdf-schema#label>, $label),
#             triple($x, $p, $org),
#             triple($org, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://xmlns.com/foaf/0.1/Organization>),
#             triple($org, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
#             triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType222>);

Q12<$label, $p, $country> :-
            triple($x, <http://www.w3.org/2000/01/rdf-schema#label>, $label),
            triple($x, $p, $org),
            triple($p, <http://www.w3.org/2000/01/rdf-schema#range>, <http://xmlns.com/foaf/0.1/Organization>),
            triple($org, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
            triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType222>);

Q12a<$label, $p, $country> :-
            triple($x, <http://www.w3.org/2000/01/rdf-schema#label>, $label),
            triple($x, $p, $org),
            triple($p, <http://www.w3.org/2000/01/rdf-schema#range>, <http://xmlns.com/foaf/0.1/Organization>),
            triple($org, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
            triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType21>);

Q12b<$label, $p, $country> :-
            triple($x, <http://www.w3.org/2000/01/rdf-schema#label>, $label),
            triple($x, $p, $org),
            triple($p, <http://www.w3.org/2000/01/rdf-schema#range>, <http://xmlns.com/foaf/0.1/Organization>),
            triple($org, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
            triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType1>);

Q13<$offer, $prop, $value> :-
              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/product>, $product),
              triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType110>),
              triple($product, $prop, $value),
              triple($prop, <http://www.w3.org/2000/01/rdf-schema#domain>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/Product>);

Q13a<$offer, $prop, $value> :-
              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/product>, $product),
              triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType8>),
              triple($product, $prop, $value),
              triple($prop, <http://www.w3.org/2000/01/rdf-schema#domain>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/Product>);

Q13b<$offer, $prop, $value> :-
              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/product>, $product),
              triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType2>),
              triple($product, $prop, $value),
              triple($prop, <http://www.w3.org/2000/01/rdf-schema#domain>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/Product>);

Q14<$product, $p, $offer, $vendor> :-
              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $p),
              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/product>, $product),
              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/vendor>, $vendor);

Q15<$review, $product, $label, $personName, $title> :-
             triple($product, <http://www.w3.org/2000/01/rdf-schema#label>, $label),
             triple($review, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/reviewFor>, $product),
             triple($review, <http://purl.org/stuff/rev#reviewer>, $person),
             triple($person, <http://xmlns.com/foaf/0.1/name>, $personName),
             triple($review, <http://purl.org/dc/elements/1.1/title>, $title);

Q16<$review, $product, $title, $text> :-
             triple($review, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/reviewFor>, $product),
             triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType2>),
             triple($review, <http://purl.org/dc/elements/1.1/title>, $title),
             triple($review, <http://purl.org/stuff/rev#text>, $text);

Q17<$review, $product, $rating> :-
             triple($review, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/reviewFor>, $product),
             triple($review, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/rating>, $rating);

Q18<$offer, $product, $label, $vendor, $price> :-
              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/product>, $product),
              triple($product, <http://www.w3.org/2000/01/rdf-schema#label>, $label),
              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/vendor>, $vendor),
              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/price>, $price);

Q19<$offer, $offerURL, $price, $deliveryDays, $value> :-
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/offerWebpage>, $offerURL),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/price>, $price),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/deliveryDays>, $deliveryDays),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/product>, $product),
             triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType32>),
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value),
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
             triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
             triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType12>);

Q19a<$offer, $offerURL, $price, $deliveryDays, $value> :-
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/offerWebpage>, $offerURL),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/price>, $price),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/deliveryDays>, $deliveryDays),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/product>, $product),
             triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType32>),
             
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value),
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
             triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
             triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType2>);

Q19b<$offer, $offerURL, $price, $deliveryDays, $value> :-
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/offerWebpage>, $offerURL),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/price>, $price),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/deliveryDays>, $deliveryDays),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/product>, $product),
             triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType3>),
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value),
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
             triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
             triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType12>);

# Q19c<$offer, $offerURL, $price, $deliveryDays, $value> :-
#              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/offerWebpage>, $offerURL),
#              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/price>, $price),
#              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/deliveryDays>, $deliveryDays),
#              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/product>, $product),
#              triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType2>),
#              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value),
#              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
#              triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
#              triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType2>);

Q20<$offer, $offerURL, $price, $deliveryDays, $value> :-
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/offerWebpage>, $offerURL),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/price>, $price),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/deliveryDays>, $deliveryDays),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/product>, $product),
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>, $productFeature),
             triple($productFeature, <http://www.w3.org/2000/01/rdf-schema#label>, "babka"),
             triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType122>),
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value),
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
             triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
             triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType12>);

Q20a<$offer, $offerURL, $price, $deliveryDays, $value> :-
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/offerWebpage>, $offerURL),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/price>, $price),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/deliveryDays>, $deliveryDays),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/product>, $product),
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>, $productFeature),
             triple($productFeature, <http://www.w3.org/2000/01/rdf-schema#label>, "babka"),
             triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType15>),
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value),
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
             triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
             triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType12>);

Q20b<$offer, $offerURL, $price, $deliveryDays, $value> :-
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/offerWebpage>, $offerURL),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/price>, $price),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/deliveryDays>, $deliveryDays),
             triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/product>, $product),
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>, $productFeature),
             triple($productFeature, <http://www.w3.org/2000/01/rdf-schema#label>, "babka"),
             triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType15>),
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value),
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
             triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
             triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType2>);

# Q20c<$offer, $offerURL, $price, $deliveryDays, $value> :-
#              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/offerWebpage>, $offerURL),
#              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/price>, $price),
#              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/deliveryDays>, $deliveryDays),
#              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/product>, $product),
#              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>, $productFeature),
#              triple($productFeature, <http://www.w3.org/2000/01/rdf-schema#label>, "babka"),
#              triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType2>),
#              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value),
#              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
#              triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
#              triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType12>);

# Q20d<$offer, $offerURL, $price, $deliveryDays, $value> :-
#              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/offerWebpage>, $offerURL),
#              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/price>, $price),
#              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/deliveryDays>, $deliveryDays),
#              triple($offer, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/product>, $product),
#              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>, $productFeature),
#              triple($productFeature, <http://www.w3.org/2000/01/rdf-schema#label>, "babka"),
#              triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType2>),
#              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric>, $value),
#              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/producer>, $producer),
#              triple($producer,<http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
#              triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType1>);

# Q21<$product, $type> :-
#              triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>, $productFeature),
#              triple($productFeature, <http://www.w3.org/2000/01/rdf-schema#label>, "babka"),
#              triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, $type);


Q22<$product, $type> :-
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>, $productFeature),
             triple($productFeature, <http://www.w3.org/2000/01/rdf-schema#label>, "babka"),
             triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, $type),
             triple($type, <http://www.w3.org/2000/01/rdf-schema#subClassOf>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType12>);

Q22a<$product, $type> :-
             triple($product, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productFeature>, $productFeature),
             triple($productFeature, <http://www.w3.org/2000/01/rdf-schema#label>, "babka"),
             triple($product, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, $type),
             triple($type, <http://www.w3.org/2000/01/rdf-schema#subClassOf>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/ProductType2>);

Q23<$x, $class, $superProp, $value> :-
                triple($x, $prop, $value),
                triple($prop, <http://www.w3.org/2000/01/rdf-schema#subPropertyOf>, $superProp),
                triple($x, $p, $agent),
                triple($p, <http://www.w3.org/2000/01/rdf-schema#range>, <http://xmlns.com/foaf/0.1/Agent>),
                triple($p, <http://www.w3.org/2000/01/rdf-schema#domain>, $class),
                triple($agent, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
                triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType12>);

Q23a<$x, $class, $superProp, $value> :-
                triple($x, $prop, $value),
                triple($prop, <http://www.w3.org/2000/01/rdf-schema#subPropertyOf>, $superProp),
                triple($x, $p, $agent),
                triple($p, <http://www.w3.org/2000/01/rdf-schema#range>, <http://xmlns.com/foaf/0.1/Agent>),
                triple($p, <http://www.w3.org/2000/01/rdf-schema#domain>, $class),
                triple($agent, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/country>, $country),
                triple($country, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/CountryType2>);
