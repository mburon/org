CREATE DATABASE bsbm1msql;

\c bsbm1msql;

DROP TABLE IF EXISTS countryType;
CREATE TABLE countryType (
  country char(2) default NULL,
  countryType integer not null,
  PRIMARY KEY (country, countryType)
) ;

INSERT INTO countryType VALUES ('CN', 111), ('KR', 111), ('US', 112), ('ES', 112), ('FR', 121), ('GB', 121), ('GB', 122), ('DE', 122), ('GB', 211), ('FR', 211), ('GB', 212), ('KR', 212), ('GB', 221), ('ES', 221), ('ES', 222), ('RU', 222);
