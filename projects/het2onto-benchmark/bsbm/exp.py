cols=[["N_ANS"]]
rewor="REW_OR/stats.csv"
ref="REF/stats.csv"
matref="MAT_REF/stats.csv"
matsat="MAT_SAT/stats.csv"
import pandas as pd
import numpy as np
import re

row_to_drop = ['Q02old', 'Q02d', 'Q02e', 'Q05', 'Q05a', 'Q05b', 'Q06', 'Q06a', 'Q06b', 'Q08', 'Q10a', 'Q10b', 'Q11', 'Q12', 'Q12a', 'Q12b', 'Q15', 'Q17', 'Q18', 'Q19b', 'Q19c', 'Q20d', 'Q23a']
#row_to_drop = ['Q02old', 'Q02d', 'Q02e',  'Q06a', 'Q06b', 'Q10a', 'Q10b', 'Q11', 'Q19b', 'Q19c', 'Q20d']
#row_to_drop = ['Q02old', 'Q02d', 'Q02e', 'Q06a', 'Q06b', 'Q10a', 'Q10b', 'Q11', 'Q15', 'Q18', 'Q19', 'Q19b', 'Q19c', 'Q23' 'Q23a']

def load_stats(f, ref_by_q, timeout = True):
    df = pd.read_csv(f, sep='\t')

    df = df[~df['INPUT'].isin(row_to_drop)]

    if not(ref_by_q is None):
        df['INPUT'] = df['INPUT'].apply(lambda q: q +  ' (' + ref_by_q[q] + ')')
    df = df.groupby(['INPUT']).mean()
    if timeout:      
        is_timeout = df['T_TOTAL'] < 600000 # 10min
        return df[is_timeout]
    else:
        return df

cols=[["N_ANS"]]
rewor="REW_OR/stats.csv"
ref="REF/stats.csv"
matref="MAT_REF/stats.csv"
matsat="MAT_SAT/stats.csv"
import matplotlib.pyplot as plt

SMALL_SIZE = 12
MEDIUM_SIZE = 16
BIGGER_SIZE = 22

plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

def print_data(data, title, isTime):
  #colors = ['#0059ad', '#ffd221', '#471203', '#6bc423', '#0053a0',  '#ffd221', '#ff410c', '#64b721']
  colors = ['#0059ad', '#ffd221', '#ff410c', '#6bc423']
  #colors = ['#006dd3', '#ffd221', '#ff410c', '#80ea2a'] 
  #colors = ['#0053a0',  '#ffd221', '#ff410c', '#64b721'] 
  #['#d72638', '#f8c07f', '#737180', '#84b3da']
  # ['#21526f', '#f36881', '#a161a2', '#ffad2e'] 
  # ['#003f5c', '#ef5675', '#955196', '#ffa600']

  plt.figure(figsize=(15,4))
  ax = data.plot.bar(figsize=(15,6), bottom=1 , width=0.8, color=colors)
  ax.xaxis.set_label_text("")

  if (isTime):
    plt.ylabel('time (ms)')
    ax.set_yscale('log')

  file_name =  title + '.png'
  # reduce the margin of the plot
  plt.savefig(file_name, bbox_inches='tight')
  return file_name

cols=[["N_ANS"]]
rewor="REW_OR/stats.csv"
ref="REF/stats.csv"
matref="MAT_REF/stats.csv"
matsat="MAT_SAT/stats.csv"
def get_ref_by_query(f):
    d = {}
    df = pd.read_csv(f, sep='\t')
    for i in df.index:
        d[df['INPUT'][i]] = str(df['N_REF'][i])
    return d

cols=[["N_ANS"]]
rewor="REW_OR/stats.csv"
ref="REF/stats.csv"
matref="MAT_REF/stats.csv"
matsat="MAT_SAT/stats.csv"
def load_total(files, col):
  dfs = []
  ref_by_q = get_ref_by_query(files['REW-CA'])
  for name, f in files.items():
    df = load_stats(f, ref_by_q)
    df = df.rename(index=str, columns={col: name})
    if (name in df.columns) :
      dfs.append(df.loc[:, name])

  return pd.concat(dfs, axis=1)

def print_total(files, col, title):
  data = load_total(files, col)

  return print_data(data, title, True)

cols=[["N_ANS"]]
rewor="REW_OR/stats.csv"
ref="REF/stats.csv"
matref="MAT_REF/stats.csv"
matsat="MAT_SAT/stats.csv"
title="rewriting-processing-time"
group=[["T_REF", "T_REW", "T_CLASH", "T_COVER", "T_CORE"]]
def load_group(files, columns):
  dfs = []
  col = 'GROUP_SUM'
  #columns.append('INPUT')
  for name, f in files.items():
    df = load_stats(f, None)
    df = df[columns]
    df[col] = df.sum(axis=1)
    df = df.rename(index=str, columns={col: name})
    dfs.append(df.loc[:, name])
  return pd.concat(dfs, axis=1)

def print_group(files, columns, title, isTime):
  data = load_group(files, columns)
  return print_data(data, title, isTime)
