Q01<$label, $featureLabel, $value> :-
              triple($product, <rdfs:label>, $label),
              triple($product, <rdf:type>, <bsbm-int:ProductType110>),
              triple($product, <bsbm:productFeature>, $productFeature),
              triple($productFeature, <rdfs:label>, $featureLabel),
              triple($product, <bsbm:productPropertyNumeric>, $value);

Q01a<$label, $featureLabel, $value> :-
              triple($product, <rdfs:label>, $label),
              triple($product, <rdf:type>, <bsbm-int:ProductType8>),
              triple($product, <bsbm:productFeature>, $productFeature),
              triple($productFeature, <rdfs:label>, $featureLabel),
              triple($product, <bsbm:productPropertyNumeric>, $value);

Q01b<$label, $featureLabel, $value> :-
              triple($product, <rdfs:label>, $label),
              triple($product, <rdf:type>, <bsbm-int:ProductType4>),
              triple($product, <bsbm:productFeature>, $productFeature),
              triple($productFeature, <rdfs:label>, $featureLabel),
              triple($product, <bsbm:productPropertyNumeric>, $value);

Q02<$label, $value, $country> :-
              triple($product, <rdfs:label>, $label),
              triple($product, <rdf:type>, <bsbm-int:ProductType110>),
              triple($product, <bsbm:productPropertyNumeric>, $value),
              triple($product, <bsbm:producer>, $producer),
              triple($producer,<bsbm:country>, $country),
              triple($country, <rdf:type>, <bsbm:CountryType12>);

Q02a<$label, $value, $country> :-
              triple($product, <rdfs:label>, $label),
              triple($product, <rdf:type>, <bsbm-int:ProductType110>),
              triple($product, <bsbm:productPropertyNumeric>, $value),
              triple($product, <bsbm:producer>, $producer),
              triple($producer,<bsbm:country>, $country),
              triple($country, <rdf:type>, <bsbm:CountryType1>);

Q02b<$label, $value, $country> :-
              triple($product, <rdfs:label>, $label),
              triple($product, <rdf:type>, <bsbm-int:ProductType8>),
              triple($product, <bsbm:productPropertyNumeric>, $value),
              triple($product, <bsbm:producer>, $producer),
              triple($producer,<bsbm:country>, $country),
              triple($country, <rdf:type>, <bsbm:CountryType1>);

Q02c<$label, $value, $country> :-
              triple($product, <rdfs:label>, $label),
              triple($product, <rdf:type>, <bsbm-int:ProductType2>),
              triple($product, <bsbm:productPropertyNumeric>, $value),
              triple($product, <bsbm:producer>, $producer),
              triple($producer,<bsbm:country>, $country),
              triple($country, <rdf:type>, <bsbm:CountryType1>);

Q03<$product1, $product2> :-
           triple($product1, <rdf:type>, <bsbm-int:ProductType12>),
           triple($product1, <bsbm:productPropertyNumeric>, "774"),
           triple($product1, <bsbm:producer>, $producer),
           triple($product2, <rdf:type>, <bsbm-int:ProductType2>),
           triple($product2, <bsbm:producer>, $producer);

Q04<$vendor, $vendorLabel, $vendorHomepage> :-
             triple($vendor, <rdfs:label>, $vendorLabel),
             triple($vendor, <foaf:homepage>, $vendorHomepage);

Q05<$offer, $offerURL, $vendor, $vendorLabel, $vendorHomepage> :-
             triple($offer, <bsbm:vendor>, $vendor),
             triple($offer, <bsbm:offerWebpage>, $offerURL),
             triple($vendor, <rdfs:label>, $vendorLabel),
             triple($vendor, <foaf:homepage>, $vendorHomepage);

Q05a<$offer, $offerURL> :-
             triple($offer, <bsbm:vendor>, $vendor),
             triple($offer, <bsbm:offerWebpage>, $offerURL),
             triple($vendor, <bsbm:country>, $country),
             triple($country, <rdf:type>, <bsbm:CountryType11>);

Q05b<$offer, $offerURL> :-
             triple($offer, <bsbm:vendor>, $vendor),
             triple($offer, <bsbm:offerWebpage>, $offerURL),
             triple($vendor, <bsbm:country>, $country),
             triple($country, <rdf:type>, <bsbm:CountryType1>);

Q06<$review, $product, $rating> :-
             triple($review, <bsbm:reviewFor>, $product),
             triple($review, <http://purl.org/stuff/rev#reviewer>, $reviewer),
             triple($review, <bsbm:rating>, $rating);

Q06a<$review, $product, $rating> :-
             triple($review, <bsbm:reviewFor>, $product),
             triple($review, <http://purl.org/stuff/rev#reviewer>, $reviewer),
             triple($review, <bsbm:rating>, $rating),
             triple($product, <bsbm:producer>, $producer),
             triple($producer,<bsbm:country>, $country),
             triple($country, <rdf:type>, <bsbm:CountryType11>);

Q06b<$review, $product, $rating> :-
             triple($review, <bsbm:reviewFor>, $product),
             triple($review, <http://purl.org/stuff/rev#reviewer>, $reviewer),
             triple($review, <bsbm:rating>, $rating),
             triple($product, <bsbm:producer>, $producer),
             triple($producer,<bsbm:country>, $country),
             triple($country, <rdf:type>, <bsbm:CountryType2>);

Q07<$country> :-
            triple($org, <rdf:type>, <foaf:Organization>),
            triple($org, <bsbm:country>, $country),
            triple($country, <rdf:type>, <bsbm:CountryType222>);

Q07a<$country> :-
            triple($org, <rdf:type>, <foaf:Organization>),
            triple($org, <bsbm:country>, $country),
            triple($country, <rdf:type>, <bsbm:CountryType22>);

Q08<$offer, $offerURL, $price, $deliveryDays> :-
             triple($offer, <bsbm:offerWebpage>, $offerURL),
             triple($offer, <bsbm:price>, $price),
             triple($offer, <bsbm:deliveryDays>, $deliveryDays);

Q09<$product, $value> :-
              triple($product, <bsbm:productPropertyNumeric>, $value);

Q10<$country, $agentType> :-
            triple($agent, <rdf:type>, $agentType),
            triple($agent, <bsbm:country>, $country),
            triple($country, <rdf:type>, <bsbm:CountryType222>);

Q10a<$country, $agentType> :-
            triple($agent, <rdf:type>, $agentType),
            triple($agent, <bsbm:country>, $country),
            triple($country, <rdf:type>, <bsbm:CountryType22>);

Q10b<$country, $agentType> :-
            triple($agent, <rdf:type>, $agentType),
            triple($agent, <bsbm:country>, $country),
            triple($country, <rdf:type>, <bsbm:CountryType2>);

Q11<$label, $p, $country> :-
            triple($x, <rdfs:label>, $label),
            triple($x, $p, $org),
            triple($org, <rdf:type>, <foaf:Organization>),
            triple($org, <bsbm:country>, $country),
            triple($country, <rdf:type>, <bsbm:CountryType222>);

Q12<$label, $p, $country> :-
            triple($x, <rdfs:label>, $label),
            triple($x, $p, $org),
            triple($p, <rdfs:range>, <foaf:Organization>),
            triple($org, <bsbm:country>, $country),
            triple($country, <rdf:type>, <bsbm:CountryType222>);

Q12a<$label, $p, $country> :-
            triple($x, <rdfs:label>, $label),
            triple($x, $p, $org),
            triple($p, <rdfs:range>, <foaf:Organization>),
            triple($org, <bsbm:country>, $country),
            triple($country, <rdf:type>, <bsbm:CountryType21>);

Q12b<$label, $p, $country> :-
            triple($x, <rdfs:label>, $label),
            triple($x, $p, $org),
            triple($p, <rdfs:range>, <foaf:Organization>),
            triple($org, <bsbm:country>, $country),
            triple($country, <rdf:type>, <bsbm:CountryType1>);

Q13<$offer, $prop, $value> :-
              triple($offer, <bsbm:product>, $product),
              triple($product, <rdf:type>, <bsbm-int:ProductType110>),
              triple($product, $prop, $value),
              triple($prop, <rdfs:domain>, <bsbm:Product>);

Q13a<$offer, $prop, $value> :-
              triple($offer, <bsbm:product>, $product),
              triple($product, <rdf:type>, <bsbm-int:ProductType8>),
              triple($product, $prop, $value),
              triple($prop, <rdfs:domain>, <bsbm:Product>);

Q13b<$offer, $prop, $value> :-
              triple($offer, <bsbm:product>, $product),
              triple($product, <rdf:type>, <bsbm-int:ProductType2>),
              triple($product, $prop, $value),
              triple($prop, <rdfs:domain>, <bsbm:Product>);

Q14<$product, $p, $offer, $vendor> :-
              triple($product, <bsbm:producer>, $p),
              triple($offer, <bsbm:product>, $product),
              triple($offer, <bsbm:vendor>, $vendor);

Q15<$review, $product, $label, $personName, $title> :-
             triple($product, <rdfs:label>, $label),
             triple($review, <bsbm:reviewFor>, $product),
             triple($review, <http://purl.org/stuff/rev#reviewer>, $person),
             triple($person, <foaf:name>, $personName),
             triple($review, <http://purl.org/dc/elements/1.1/title>, $title);

Q16<$review, $product, $title, $text> :-
             triple($review, <bsbm:reviewFor>, $product),
             triple($product, <rdf:type>, <bsbm-int:ProductType2>),
             triple($review, <http://purl.org/dc/elements/1.1/title>, $title),
             triple($review, <http://purl.org/stuff/rev#text>, $text);

Q17<$review, $product, $rating> :-
             triple($review, <bsbm:reviewFor>, $product),
             triple($review, <bsbm:rating>, $rating);

Q18<$offer, $product, $label, $vendor, $price> :-
              triple($offer, <bsbm:product>, $product),
              triple($product, <rdfs:label>, $label),
              triple($offer, <bsbm:vendor>, $vendor),
              triple($offer, <bsbm:price>, $price);

Q19<$offer, $offerURL, $price, $deliveryDays, $value> :-
             triple($offer, <bsbm:offerWebpage>, $offerURL),
             triple($offer, <bsbm:price>, $price),
             triple($offer, <bsbm:deliveryDays>, $deliveryDays),
             triple($offer, <bsbm:product>, $product),
             triple($product, <rdf:type>, <bsbm-int:ProductType8>),
             triple($product, <bsbm:productPropertyNumeric>, $value),
             triple($product, <bsbm:producer>, $producer),
             triple($producer,<bsbm:country>, $country),
             triple($country, <rdf:type>, <bsbm:CountryType12>);

Q19a<$offer, $offerURL, $price, $deliveryDays, $value> :-
             triple($offer, <bsbm:offerWebpage>, $offerURL),
             triple($offer, <bsbm:price>, $price),
             triple($offer, <bsbm:deliveryDays>, $deliveryDays),
             triple($offer, <bsbm:product>, $product),
             triple($product, <rdf:type>, <bsbm-int:ProductType8>),
             
             triple($product, <bsbm:productPropertyNumeric>, $value),
             triple($product, <bsbm:producer>, $producer),
             triple($producer,<bsbm:country>, $country),
             triple($country, <rdf:type>, <bsbm:CountryType2>);

Q19b<$offer, $offerURL, $price, $deliveryDays, $value> :-
             triple($offer, <bsbm:offerWebpage>, $offerURL),
             triple($offer, <bsbm:price>, $price),
             triple($offer, <bsbm:deliveryDays>, $deliveryDays),
             triple($offer, <bsbm:product>, $product),
             triple($product, <rdf:type>, <bsbm-int:ProductType3>),
             triple($product, <bsbm:productPropertyNumeric>, $value),
             triple($product, <bsbm:producer>, $producer),
             triple($producer,<bsbm:country>, $country),
             triple($country, <rdf:type>, <bsbm:CountryType12>);

Q20<$offer, $offerURL, $price, $deliveryDays, $value> :-
             triple($offer, <bsbm:offerWebpage>, $offerURL),
             triple($offer, <bsbm:price>, $price),
             triple($offer, <bsbm:deliveryDays>, $deliveryDays),
             triple($offer, <bsbm:product>, $product),
             triple($product, <bsbm:productFeature>, $productFeature),
             triple($productFeature, <rdfs:label>, "biographies"),
             triple($product, <rdf:type>, <bsbm-int:ProductType110>),
             triple($product, <bsbm:productPropertyNumeric>, $value),
             triple($product, <bsbm:producer>, $producer),
             triple($producer,<bsbm:country>, $country),
             triple($country, <rdf:type>, <bsbm:CountryType12>);

Q20a<$offer, $offerURL, $price, $deliveryDays, $value> :-
             triple($offer, <bsbm:offerWebpage>, $offerURL),
             triple($offer, <bsbm:price>, $price),
             triple($offer, <bsbm:deliveryDays>, $deliveryDays),
             triple($offer, <bsbm:product>, $product),
             triple($product, <bsbm:productFeature>, $productFeature),
             triple($productFeature, <rdfs:label>, "biographies"),
             triple($product, <rdf:type>, <bsbm-int:ProductType8>),
             triple($product, <bsbm:productPropertyNumeric>, $value),
             triple($product, <bsbm:producer>, $producer),
             triple($producer,<bsbm:country>, $country),
             triple($country, <rdf:type>, <bsbm:CountryType12>);

Q20b<$offer, $offerURL, $price, $deliveryDays, $value> :-
             triple($offer, <bsbm:offerWebpage>, $offerURL),
             triple($offer, <bsbm:price>, $price),
             triple($offer, <bsbm:deliveryDays>, $deliveryDays),
             triple($offer, <bsbm:product>, $product),
             triple($product, <bsbm:productFeature>, $productFeature),
             triple($productFeature, <rdfs:label>, "biographies"),
             triple($product, <rdf:type>, <bsbm-int:ProductType2>),
             triple($product, <bsbm:productPropertyNumeric>, $value),
             triple($product, <bsbm:producer>, $producer),
             triple($producer,<bsbm:country>, $country),
             triple($country, <rdf:type>, <bsbm:CountryType12>);

Q20c<$offer, $offerURL, $price, $deliveryDays, $value> :-
             triple($offer, <bsbm:offerWebpage>, $offerURL),
             triple($offer, <bsbm:price>, $price),
             triple($offer, <bsbm:deliveryDays>, $deliveryDays),
             triple($offer, <bsbm:product>, $product),
             triple($product, <bsbm:productFeature>, $productFeature),
             triple($productFeature, <rdfs:label>, "biographies"),
             triple($product, <rdf:type>, <bsbm-int:ProductType2>),
             triple($product, <bsbm:productPropertyNumeric>, $value),
             triple($product, <bsbm:producer>, $producer),
             triple($producer,<bsbm:country>, $country),
             triple($country, <rdf:type>, <bsbm:CountryType1>);

Q21<$product, $type> :-
             triple($product, <bsbm:productFeature>, $productFeature),
             triple($productFeature, <rdfs:label>, "biographies"),
             triple($product, <rdf:type>, $type);

Q22<$product, $type> :-
             triple($product, <bsbm:productFeature>, $productFeature),
             triple($productFeature, <rdfs:label>, "biographies"),
             triple($product, <rdf:type>, $type),
             triple($type, <rdfs:subClassOf>, <bsbm-int:ProductType12>);

Q22a<$product, $type> :-
             triple($product, <bsbm:productFeature>, $productFeature),
             triple($productFeature, <rdfs:label>, "biographies"),
             triple($product, <rdf:type>, $type),
             triple($type, <rdfs:subClassOf>, <bsbm-int:ProductType2>);

Q23<$x, $class, $superProp, $value> :-
                triple($x, $prop, $value),
                triple($prop, <rdfs:subPropertyOf>, $superProp),
                triple($x, $p, $agent),
                triple($p, <rdfs:range>, <foaf:Agent>),
                triple($p, <rdfs:domain>, $class),
                triple($agent, <bsbm:country>, $country),
                triple($country, <rdf:type>, <bsbm:CountryType12>);

Q23a<$x, $class, $superProp, $value> :-
                triple($x, $prop, $value),
                triple($prop, <rdfs:subPropertyOf>, $superProp),
                triple($x, $p, $agent),
                triple($p, <rdfs:range>, <foaf:Agent>),
                triple($p, <rdfs:domain>, $class),
                triple($agent, <bsbm:country>, $country),
                triple($country, <rdf:type>, <bsbm:CountryType2>);
