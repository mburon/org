#+TITLE: Query Answering Test

* Test graph

There are 12 initial triples in the graph:

#+BEGIN_SRC shell :results output :exports results
cat qa-test.nt
#+END_SRC

#+RESULTS:
#+begin_example
<Maxime> <phoneNumber> "245470000" .
<Maxime> <livesIn> <Orsay> .
<Ioana> <livesIn> <Palaiseau> .
<Ioana> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <Person> .
<François> <livesIn> <Lannion> .
<François> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <Person> .
<Marie-Laure> <livesIn> <Montpellier> .
<Marie-Laure> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <Person> .
<Thing> <http://www.w3.org/2000/01/rdf-schema#subClassOf> <Thing> .
<Person> <http://www.w3.org/2000/01/rdf-schema#subClassOf> <Thing> .
<Place> <http://www.w3.org/2000/01/rdf-schema#subClassOf> <Thing> .
<City> <http://www.w3.org/2000/01/rdf-schema#subClassOf> <Place> .
<livesIn> <http://www.w3.org/2000/01/rdf-schema#range> <City> .
<contact> <http://www.w3.org/2000/01/rdf-schema#domain> <Person> .
<phoneNumber> <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> <contact> .
<contact> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/1999/02/22-rdf-syntax-ns#Property> .
<Place> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2000/01/rdf-schema#Class> .
#+end_example

#+BEGIN_SRC shell :exports results
rdf_img qa-test.nt qa-test.dot

sed -i 's/http:\/\/example\.org\///g' qa-test.dot
sed -i 's/rdf://g' qa-test.dot
sed -i 's/http:\/\/www\.w3\.org\/2000\/01\/rdf-schema#//g' qa-test.dot
sed -i 's/%C3%A7/ç/g' qa-test.dot

dot -Tpng qa-test.dot > qa-test.png
#+END_SRC

#+RESULTS:

[[file:qa-test.png]]

** Schema

Overview of the schema 

#+BEGIN_SRC shell :results output :exports results
grep "<http://www.w3.org/2000/01/rdf-schema#subClassOf>\|<http://www.w3.org/2000/01/rdf-schema#subPropertyOf>\|<http://www.w3.org/2000/01/rdf-schema#range>\|<http://www.w3.org/2000/01/rdf-schema#domain>\|<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/1999/02/22-rdf-syntax-ns#Property>\|<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2000/01/rdf-schema#Class>" qa-test.nt > qa-test-schema.nt

rdf_img qa-test-schema.nt qa-test-schema.dot

sed -i 's/http:\/\/example\.org\///g' qa-test-schema.dot
sed -i 's/rdf://g' qa-test-schema.dot
sed -i 's/http:\/\/www\.w3\.org\/2000\/01\/rdf-schema#//g' qa-test-schema.dot

dot -Tpng qa-test-schema.dot > qa-test-schema.png

graph-easy qa-test-schema.dot
#+END_SRC

#+RESULTS:
#+begin_example
                                                                                         subClassOf
                                                                                       +------------+
                                                                                       v            |
+-------------+  subPropertyOf   +----------+  domain       +--------+  subClassOf   +----------------+
| phoneNumber | ---------------> | contact  | ------------> | Person | ------------> |     Thing      |
+-------------+                  +----------+               +--------+               +----------------+
                                   |                                                   ^
                                   | type                                              |
                                   v                                                   |
+-------------+                  +----------+                                          |
|   livesIn   |                  | Property |                                          |
+-------------+                  +----------+                                          |
  |                                                                                    |
  | range                                                                              |
  v                                                                                    |
+-------------+  subClassOf      +----------+  subClassOf                              |
|    City     | ---------------> |  Place   | -----------------------------------------+
+-------------+                  +----------+
                                   |
                                   | type
                                   v
                                 +----------+
                                 |  Class   |
                                 +----------+
#+end_example



* Scenarios

** Evaluation
In this scenario, we simply evaluate the query on the graph described above without reasoning. 

*** Tests

#+BEGIN_SRC java
    /**
     * 1 answer: 
     *   - <> 
     */
    @Test
    public void shouldAnswerBooleanQuery() throws Exception {
        Set<Tuple<Term>> answers = new HashSet<Tuple<Term>>();
        Tuple t = new Tuple();
        answers.add(t);

        String queryString = "Q<> :- triple(<Maxime>, <livesIn>, <Orsay>) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 0 answer: 
     */
    @Test
    public void shouldAnswerBooleanQueryWithInvalidType() throws Exception {
        Set<Tuple<Term>> answers = new HashSet<Tuple<Term>>();

        String queryString = "Q<> :- triple($s, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <Orsay>) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 0 answer: 
     */
    @Test
    public void shouldAnswerBooleanQueryWithVariableAsSubject() throws Exception {
        Set<Tuple<Term>> answers = new HashSet<Tuple<Term>>();
        Tuple t = new Tuple();

        String queryString = "Q<> :- triple($s, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <City>) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 0 answer
     */
    @Test
    public void shouldAnswerBooleanQueryWithVariableAsProperty() throws Exception {
        Set<Tuple<Term>> answers = new HashSet<Tuple<Term>>();

        String queryString = "Q<> :- triple(<Maxime>, $p, <Montpellier>) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 0 answer: 
     */
    @Test
    public void shouldAnswerBooleanQueryWithVariableAsObject() throws Exception {
        Set<Tuple<Term>> answers = new HashSet<Tuple<Term>>();

        String queryString = "Q<> :- triple(<Orsay>, <contact>, $o) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer: 
     *   - <Maxime> 
     */
    @Test
    public void shouldSelectSubject() throws Exception {
        TupleSet answers = new TupleSet();
        Tuple t = new Tuple(new IRI("Maxime"));
        answers.add(t);

        String queryString = "Q<$subject> :- triple($subject, <livesIn>, <Orsay>) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer: 
     *   - <livesIn>
     */
    @Test
    public void shouldSelectProperty() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("livesIn")));

        String queryString = "Q<$prop> :- triple(<Maxime>, $prop, <Orsay>) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer: 
     *   - <Orsay>
     */
    @Test
    public void shouldSelectObject() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Orsay")));

        String queryString = "Q<$object> :- triple(<Maxime>, <livesIn>, $object) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer: 
     *   - "245470000"
     */
    @Test
    public void shouldSelectLiteralObject() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new Literal("245470000")));

        String queryString = "Q<$object> :- triple(<Maxime>, <phoneNumber>, $object) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer: 
     *   - <Maxime>
     */
    @Test
    public void shouldSelectSubjectWithExistentialProperty() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Maxime")));

        String queryString = "Q<$subject> :- triple($subject, $prop, <Orsay>) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer: 
     *   - <Maxime>
     */
    @Test
    public void shouldSelectSubjectWithExistentialObject() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Maxime")));

        String queryString = "Q<$subject> :- triple($subject, <phoneNumber>, $object) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer: 
     *   - <phoneNumber>
     */
    @Test
    public void shouldSelectPropertyWithExistentialSubject() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("phoneNumber")));

        String queryString = "Q<$prop> :- triple($subject, $prop, \"245470000\") ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 3 answers: 
     *   - <phoneNumber>
     *   - <livesIn>
     *   - <rdf:type>
     */
    @Test
    public void shouldSelectPropertyWithExistentialObject() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("phoneNumber")));
        answers.add(new Tuple(new IRI("livesIn")));
        answers.add(new Tuple(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")));
        
        String queryString = "Q<$prop> :- triple(<Maxime>, $prop, $object) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 4 answers: 
     *   - <Orsay>
     *   - <Palaiseau>
     *   - <Montpellier>
     *   - <Lannion>
     */
    @Test
    public void shouldSelectObjectWithExistentialSubject() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Orsay")));
        answers.add(new Tuple(new IRI("Palaiseau")));
        answers.add(new Tuple(new IRI("Montpellier")));
        answers.add(new Tuple(new IRI("Lannion")));

        String queryString = "Q<$object> :- triple($subject, <livesIn>, $object) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 2 answers: 
     *   - "245470000"
     *   - <Orsay>
     */
    @Test
    public void shouldSelectObjectWithExistentialProperty() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new Literal("245470000")));
        answers.add(new Tuple(new IRI("Orsay")));

        String queryString = "Q<$object> :- triple(<Maxime>, $prop, $object) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * no answer 
     */
    @Test
    public void shouldSelectSubjectOfType() throws Exception {
        TupleSet answers = new TupleSet();

        String queryString = "Q<$subject> :- triple($subject, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <Place>);";
        assertAnswer(queryString, answers);
    }

    /**
     *  no answer: 
     */
    @Test
    public void shouldSelectSubjectOfType2() throws Exception {
        TupleSet answers = new TupleSet();

        String queryString = "Q<$subject> :- triple($subject, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <Thing>);";
        assertAnswer(queryString, answers);
    }

    /**
     * 3 answers:
     */
    @Test
    public void shouldSelectSubjectOfType3() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Marie-Laure")));
        answers.add(new Tuple(new IRI("Ioana")));
        answers.add(new Tuple(new IRI("François")));

        String queryString = "Q<$subject> :- triple($subject, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <Person>);";
        assertAnswer(queryString, answers);
    }

    /**
     * no answer: 
     */
    @Test
    public void shouldSelectTypeOfSubject() throws Exception {
        TupleSet answers = new TupleSet();
        
        String queryString = "Q<$type> :- triple(<Maxime>, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, $type);";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer: 
     */
    @Test
    public void shouldSelectTypeOfSubject2() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Person")));
        
        String queryString = "Q<$type> :- triple(<Ioana>, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, $type);";
        assertAnswer(queryString, answers);
    }

    /**
     * 3 answers: 
     *   - <Person>
     *   - <Class>
     *   - <Property>
     */
    @Test
    public void shouldSelectAllTypes() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Person")));
        answers.add(new Tuple(new IRI("http://www.w3.org/2000/01/rdf-schema#Class")));
        answers.add(new Tuple(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#Property")));
        
        String queryString = "Q<$type> :- triple($subject, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, $type);";
        assertAnswer(queryString, answers);
    }

    /**
     * 7 answers: 
     *   - <rdf:type>
     *   - <phoneNumber>
     *   - <livesIn>
     *   - <rdfs:subClassOf>
     *   - <rdfs:subProperty>
     *   - <rdfs:domain>
     *   - <rdfs:range>
     */
    @Test
    public void shouldSelectAllProperties() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")));
        answers.add(new Tuple(new IRI("phoneNumber")));
        answers.add(new Tuple(new IRI("livesIn")));
        answers.add(new Tuple(new IRI("http://www.w3.org/2000/01/rdf-schema#subClassOf")));
        answers.add(new Tuple(new IRI("http://www.w3.org/2000/01/rdf-schema#subPropertyOf")));
        answers.add(new Tuple(new IRI("http://www.w3.org/2000/01/rdf-schema#domain")));
        answers.add(new Tuple(new IRI("http://www.w3.org/2000/01/rdf-schema#range")));
        
        String queryString = "Q<$prop> :- triple($subject, $prop, $object);";
        assertAnswer(queryString, answers);
    }

    /**
     * no answer:
     */
    @Test
    public void shouldJoin() throws Exception {
        TupleSet answers = new TupleSet();

        String queryString = "Q<$city, $number> :- triple($name, <livesIn>, $city), triple($name, <contact>, $number) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer:
     */
    @Test
    public void shouldJoin2() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Orsay"), new Literal("245470000")));

        String queryString = "Q<$city, $number> :- triple($name, <livesIn>, $city), triple($name, <phoneNumber>, $number) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answers:
     *   - <Thing>, <subClassOf>
     */
    @Test
    public void shouldSelectOnVariableEquality() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Thing"), new IRI("http://www.w3.org/2000/01/rdf-schema#subClassOf")));

        String queryString = "Q<$var, $prop> :- triple($var, $prop, $var);";
        assertAnswer(queryString, answers);
    }

    /**
     * no answer
     *
     */
    @Test
    public void shouldDoProduct() throws Exception {
        TupleSet answers = new TupleSet();
        
        String queryString = "Q<$prop, $city> :- triple(<Ioana>, $prop, $object), triple($city,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <City>);";
        assertAnswer(queryString, answers);
    }

    
    /**
     * 8 answer
     *
     */
    @Test
    public void shouldDoProduct2() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), new IRI("Orsay")));
        answers.add(new Tuple(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), new IRI("Lannion")));
        answers.add(new Tuple(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), new IRI("Palaiseau")));
        answers.add(new Tuple(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), new IRI("Montpellier")));
        answers.add(new Tuple(new IRI("livesIn"), new IRI("Orsay")));
        answers.add(new Tuple(new IRI("livesIn"), new IRI("Lannion")));
        answers.add(new Tuple(new IRI("livesIn"), new IRI("Palaiseau")));
        answers.add(new Tuple(new IRI("livesIn"), new IRI("Montpellier")));
        
        String queryString = "Q<$prop, $city> :- triple(<Ioana>, $prop, $object), triple($name ,<livesIn>, $city);";
        assertAnswer(queryString, answers);
    }
#+END_SRC

** Full RDFS Reasoning

*** Overview

The fully saturated graph looks like ([[file:qa-test-full-saturation.nt][nt file]]):

#+BEGIN_SRC shell :exports results
rdf_img qa-test-full-saturation.nt qa-test-full-saturation.dot

sed -i 's/http:\/\/example\.org\///g' qa-test-full-saturation.dot
sed -i 's/rdf://g' qa-test-full-saturation.dot
sed -i 's/http:\/\/www\.w3\.org\/2000\/01\/rdf-schema#//g' qa-test-full-saturation.dot
sed -i 's/%C3%A7/ç/g' qa-test-full-saturation.dot
dot -Tpng qa-test-full-saturation.dot > qa-test-full-saturation.png

#echo 'qa-test-full-saturation.png'
#+END_SRC

#+RESULTS:


[[file:qa-test-full-saturation.png]]

*** Implementations

It concerns the following settings in ~query-session~:
- ~TranslatedUCQuery~ with ~FULL_REAS~ reasoner
- ~TranslatedJUCQuery~ with ~FULL_REAS~ reasoner
- ~TranslatedSCQuery~ with ~FULL_REAS~ reasoner

*** Tests

#+begin_SRC java
    /**
     * 1 answer: 
     *   - <> 
     */
    @Test
    public void shouldAnswerBooleanQuery() throws Exception {
        Set<Tuple<Term>> answers = new HashSet<Tuple<Term>>();
        Tuple t = new Tuple();
        answers.add(t);

        String queryString = "Q<> :- triple(<Maxime>, <livesIn>, <Orsay>) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 0 answer: 
     */
    @Test
    public void shouldAnswerBooleanQueryWithInvalidType() throws Exception {
        Set<Tuple<Term>> answers = new HashSet<Tuple<Term>>();

        String queryString = "Q<> :- triple($s, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <Orsay>) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer: 
     * - <>
     */
    @Test
    public void shouldAnswerBooleanQueryWithVariableAsSubject() throws Exception {
        Set<Tuple<Term>> answers = new HashSet<Tuple<Term>>();
        Tuple t = new Tuple();
        answers.add(t);

        String queryString = "Q<> :- triple($s, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <City>) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 0 answer
     */
    @Test
    public void shouldAnswerBooleanQueryWithVariableAsProperty() throws Exception {
        Set<Tuple<Term>> answers = new HashSet<Tuple<Term>>();

        String queryString = "Q<> :- triple(<Maxime>, $p, <Montpellier>) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 0 answer: 
     */
    @Test
    public void shouldAnswerBooleanQueryWithVariableAsObject() throws Exception {
        Set<Tuple<Term>> answers = new HashSet<Tuple<Term>>();

        String queryString = "Q<> :- triple(<Orsay>, <contact>, $o) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer: 
     *   - <Maxime> 
     */
    @Test
    public void shouldSelectSubject() throws Exception {
        TupleSet answers = new TupleSet();
        Tuple t = new Tuple(new IRI("Maxime"));
        answers.add(t);

        String queryString = "Q<$subject> :- triple($subject, <livesIn>, <Orsay>) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer: 
     *   - <livesIn>
     */
    @Test
    public void shouldSelectProperty() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("livesIn")));

        String queryString = "Q<$prop> :- triple(<Maxime>, $prop, <Orsay>) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer: 
     *   - <Orsay>
     */
    @Test
    public void shouldSelectObject() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Orsay")));

        String queryString = "Q<$object> :- triple(<Maxime>, <livesIn>, $object) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer: 
     *   - "245470000"
     */
    @Test
    public void shouldSelectLiteralObject() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new Literal("245470000")));

        String queryString = "Q<$object> :- triple(<Maxime>, <phoneNumber>, $object) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer: 
     *   - <Maxime>
     */
    @Test
    public void shouldSelectSubjectWithExistentialProperty() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Maxime")));

        String queryString = "Q<$subject> :- triple($subject, $prop, <Orsay>) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer: 
     *   - <Maxime>
     */
    @Test
    public void shouldSelectSubjectWithExistentialObject() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Maxime")));

        String queryString = "Q<$subject> :- triple($subject, <phoneNumber>, $object) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 2 answers: 
     *   - <contact>
     *   - <phoneNumber>
     */
    @Test
    public void shouldSelectPropertyWithExistentialSubject() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("contact")));
        answers.add(new Tuple(new IRI("phoneNumber")));

        String queryString = "Q<$prop> :- triple($subject, $prop, \"245470000\") ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 4 answers: 
     *   - <contact>
     *   - <phoneNumber>
     *   - <livesIn>
     *   - <rdf:type>
     */
    @Test
    public void shouldSelectPropertyWithExistentialObject() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("contact")));
        answers.add(new Tuple(new IRI("phoneNumber")));
        answers.add(new Tuple(new IRI("livesIn")));
        answers.add(new Tuple(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")));
        
        String queryString = "Q<$prop> :- triple(<Maxime>, $prop, $object) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 4 answers: 
     *   - <Orsay>
     *   - <Palaiseau>
     *   - <Montpellier>
     *   - <Lannion>
     */
    @Test
    public void shouldSelectObjectWithExistentialSubject() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Orsay")));
        answers.add(new Tuple(new IRI("Palaiseau")));
        answers.add(new Tuple(new IRI("Montpellier")));
        answers.add(new Tuple(new IRI("Lannion")));

        String queryString = "Q<$object> :- triple($subject, <livesIn>, $object) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 4 answers: 
     *   - <Thing>
     *   - <Person>
     *   - "245470000"
     *   - <Orsay>
     */
    @Test
    public void shouldSelectObjectWithExistentialProperty() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Thing")));
        answers.add(new Tuple(new IRI("Person")));
        answers.add(new Tuple(new Literal("245470000")));
        answers.add(new Tuple(new IRI("Orsay")));

        String queryString = "Q<$object> :- triple(<Maxime>, $prop, $object) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 4 answers: 
     *   - <Orsay>
     *   - <Palaiseau>
     *   - <Montpellier>
     *   - <Lannion>
     */
    @Test
    public void shouldSelectSubjectOfType() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Orsay")));
        answers.add(new Tuple(new IRI("Palaiseau")));
        answers.add(new Tuple(new IRI("Montpellier")));
        answers.add(new Tuple(new IRI("Lannion")));

        String queryString = "Q<$subject> :- triple($subject, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <Place>);";
        assertAnswer(queryString, answers);
    }

    /**
     *  8 answers: 
     *   - <Orsay>
     *   - <Palaiseau>
     *   - <Montpellier>
     *   - <Lannion>
     *   - <Maxime>
     *   - <Marie-Laure>
     *   - <Ioana>
     *   - <François>
     */
    @Test
    public void shouldSelectSubjectOfType2() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Orsay")));
        answers.add(new Tuple(new IRI("Palaiseau")));
        answers.add(new Tuple(new IRI("Montpellier")));
        answers.add(new Tuple(new IRI("Lannion")));
        answers.add(new Tuple(new IRI("Maxime")));
        answers.add(new Tuple(new IRI("Marie-Laure")));
        answers.add(new Tuple(new IRI("Ioana")));
        answers.add(new Tuple(new IRI("François")));

        String queryString = "Q<$subject> :- triple($subject, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <Thing>);";
        assertAnswer(queryString, answers);
    }

    /**
     * 2 answers: 
     *   - <Person>
     *   - <Thing>
     */
    @Test
    public void shouldSelectTypeOfSubject() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Person")));
        answers.add(new Tuple(new IRI("Thing")));
        
        String queryString = "Q<$type> :- triple(<Maxime>, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, $type);";
        assertAnswer(queryString, answers);
    }
    
    /**
     * 6 answers: 
     *   - <Person>
     *   - <Thing>
     *   - <Place>
     *   - <City>
     *   - <Class>
     *   - <Property>
     */
    @Test
    public void shouldSelectAllTypes() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Person")));
        answers.add(new Tuple(new IRI("Thing")));
        answers.add(new Tuple(new IRI("Place")));
        answers.add(new Tuple(new IRI("City")));
        answers.add(new Tuple(new IRI("http://www.w3.org/2000/01/rdf-schema#Class")));
        answers.add(new Tuple(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#Property")));
        
        String queryString = "Q<$type> :- triple($subject, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, $type);";
        assertAnswer(queryString, answers);
    }

    /**
     * 8 answers: 
     *   - <rdf:type>
     *   - <phoneNumber>
     *   - <contact>
     *   - <livesIn>
     *   - <rdfs:subClassOf>
     *   - <rdfs:subProperty>
     *   - <rdfs:domain>
     *   - <rdfs:range>
     */
    @Test
    public void shouldSelectAllProperties() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")));
        answers.add(new Tuple(new IRI("phoneNumber")));
        answers.add(new Tuple(new IRI("contact")));
        answers.add(new Tuple(new IRI("livesIn")));
        answers.add(new Tuple(new IRI("http://www.w3.org/2000/01/rdf-schema#subClassOf")));
        answers.add(new Tuple(new IRI("http://www.w3.org/2000/01/rdf-schema#subPropertyOf")));
        answers.add(new Tuple(new IRI("http://www.w3.org/2000/01/rdf-schema#domain")));
        answers.add(new Tuple(new IRI("http://www.w3.org/2000/01/rdf-schema#range")));
        
        String queryString = "Q<$prop> :- triple($subject, $prop, $object);";
        assertAnswer(queryString, answers);
    }

    /**
     * 33 answers
     * We found 39 triples, because of this issue https://gitlab.inria.fr/cedar/reasoning/issues/2,
     * the 6 added triples are the following: 
     * - | <phoneNumber> | <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> | <phoneNumber> |,
     * - | <City> | <http://www.w3.org/2000/01/rdf-schema#subClassOf> | <City> |,
     * - | <Person> | <http://www.w3.org/2000/01/rdf-schema#subClassOf> | <Person> |,
     * - | <Place> | <http://www.w3.org/2000/01/rdf-schema#subClassOf> | <Place> |,
     * - | <contact> | <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> | <contact> |,
     * - | <livesIn> | <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> | <livesIn> |,
     * 
     */
    // @Test
    // public void shouldSelectEveryTriple() throws Exception {
    //     String queryString = "Q<$subject, $prop, $object> :- triple($subject, $prop, $object);";
    //     assertAnswerNumber(queryString, 33);
    // }

    /**
     * 1 answers:
     *   - <Orsay>, "245470000"
     */
    @Test
    public void shouldJoin() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Orsay"), new Literal("245470000")));

        String queryString = "Q<$city, $number> :- triple($name, <livesIn>, $city), triple($name, <contact>, $number) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answers:
     *   - <Thing>, <subClassOf>
     */
    @Test
    public void shouldSelectOnVariableEquality() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Thing"), new IRI("http://www.w3.org/2000/01/rdf-schema#subClassOf")));

        String queryString = "Q<$var, $prop> :- triple($var, $prop, $var);";
        assertAnswer(queryString, answers);
    }

    /**
     * 8 answers
     *
     */
    @Test
    public void shouldDoProduct() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), new IRI("Orsay")));
        answers.add(new Tuple(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), new IRI("Lannion")));
        answers.add(new Tuple(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), new IRI("Palaiseau")));
        answers.add(new Tuple(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), new IRI("Montpellier")));
        answers.add(new Tuple(new IRI("livesIn"), new IRI("Orsay")));
        answers.add(new Tuple(new IRI("livesIn"), new IRI("Lannion")));
        answers.add(new Tuple(new IRI("livesIn"), new IRI("Palaiseau")));
        answers.add(new Tuple(new IRI("livesIn"), new IRI("Montpellier")));
        
        String queryString = "Q<$prop, $city> :- triple(<Ioana>, $prop, $object), triple($city,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <City>);";
        assertAnswer(queryString, answers);
    }

    @Test
    public void shouldFindAllSubPropertyOf() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("phoneNumber"), new IRI("contact")));

        String queryString = "Q<$X, $Y> :- triple($X, <http://www.w3.org/2000/01/rdf-schema#subPropertyOf>, $Y);";

        assertAnswer(queryString, answers);
    }

    @Test
    public void shouldFindAllSubClassOf() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("Person"), new IRI("Thing")));
        answers.add(new Tuple(new IRI("Thing"), new IRI("Thing")));
        answers.add(new Tuple(new IRI("City"), new IRI("Thing")));
        answers.add(new Tuple(new IRI("City"), new IRI("Place")));
        answers.add(new Tuple(new IRI("Place"), new IRI("Thing")));
        String queryString = "Q<$X, $Y> :- triple($X, <http://www.w3.org/2000/01/rdf-schema#subClassOf>, $Y);";

        assertAnswer(queryString, answers);
    }

    @Test
    public void shouldFindAllDomain() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("phoneNumber"), new IRI("Thing")));
        answers.add(new Tuple(new IRI("phoneNumber"), new IRI("Person")));
        answers.add(new Tuple(new IRI("contact"), new IRI("Thing")));
        answers.add(new Tuple(new IRI("contact"), new IRI("Person")));
        String queryString = "Q<$X, $Y> :- triple($X, <http://www.w3.org/2000/01/rdf-schema#domain>, $Y);";

        assertAnswer(queryString, answers);
    }

    @Test
    public void shouldFindAllRange() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("livesIn"), new IRI("City")));
        answers.add(new Tuple(new IRI("livesIn"), new IRI("Place")));
        answers.add(new Tuple(new IRI("livesIn"), new IRI("Thing")));
        String queryString = "Q<$X, $Y> :- triple($X, <http://www.w3.org/2000/01/rdf-schema#range>, $Y);";

        assertAnswer(queryString, answers);
    }

    @Test
    public void shouldFindAnswersOfQueryWithEmptyBody() throws Exception {
        TupleSet answers = new TupleSet();
        answers.add(new Tuple(new IRI("test")));

        String queryString = "Q<<test>> :- ;";

        assertAnswer(queryString, answers);
    }

#+END_SRC
