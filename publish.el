
;;--- general org configuration ---

;; disable skiping by flag
(setq org-publish-use-timestamps-flag t)

;; customization of time stamp display
;; 17/02/2019
(setq-default org-display-custom-times t)
(setq org-time-stamp-custom-formats '("<%d/%m/%Y>" . "<%d/%m/%Y %H:%M>"))

;; remove <> around time stamps

(add-to-list 'org-export-filter-timestamp-functions
             #'inria-website-filter-timestamp)

(defun inria-website-filter-timestamp (trans back _comm)
  "Remove <> around time-stamps."
  (pcase back
    ((or `jekyll `html)
     (replace-regexp-in-string "&[lg]t;" "" trans))
    (`latex
     (replace-regexp-in-string "[<>]" "" trans))))

(defvar inria-website-TeX-macros-path
  "~/inria/org/macros.tex")

;; head and header links are absolute relatively to the base
;; of the website, so we just need to change the base from dev to prod

(setq inria-website-url-base "https://pages.saclay.inria.fr/maxime.buron")

(defun inria-website-html-head (base)
  (concat " <link rel='shortcut icon' href='" base "/img/favicon.ico'/> 
<link href='" base "/css/style.css' rel='stylesheet' type='text/css'>"))

(defun inria-website-html-nav (base)
  (concat
   "<div class='nav'>
<ul>
<li><a href='" base "/index.html'>Home</a></li>
<li><a href='" base "/reviews.html'>Reviews</a></li>
<li><a href='" base "/posts.html'>Posts</a></li>
<li><a href='" base "/projects/'>Projects</a></li>
</ul>
</div>"))

(defun get-string-from-file (filePath)
  "Return filePath's file content."
  (with-temp-buffer
    (insert-file-contents filePath)
    (buffer-string)))

(defun get-TeX-macros-string ()
  "get TeX macro file as string
escape some problematic character"
  (replace-regexp-in-string
   "%"
   "%%" 
   (replace-regexp-in-string "\\\\" "\\\\" (get-string-from-file inria-website-TeX-macros-path))))

(defun get-TeX-macros-html ()
  "get a HTML tag containing TeX macros"
  (concat "<div style='display:none;'> $$" (get-TeX-macros-string) "$$ </div>"))

(defun get-preambule-html (base)
  "get HTML preambule"
  (concat (inria-website-html-nav base) (get-TeX-macros-html)))

(defun inria-website-create-project-configuration (suffix publishing-dir base)
  "create a configuration for inria website according to publishing-dir and base"
  `((,(concat "inria-website-html-" suffix)
     :base-directory "~/inria/org/"
     :publishing-directory ,publishing-dir
     :publishing-function org-html-publish-to-html
     :recursive t
     :html-toplevel-hlevel 2
     :section-numbers nil
     :with-toc nil
     :with-author nil
     :html-head ,(inria-website-html-head base)
     :html-preamble ,(get-preambule-html base)
     :html-postamble nil)
    (,(concat "inria-website-static-" suffix)
     :base-directory "~/inria/org/"
     :base-extension "png\\|css"
     :publishing-directory ,publishing-dir
     :publishing-function org-publish-attachment
     :recursive t
     :exclude "\.git/.*")
    (,(concat "inria-website-" suffix)
     :components (,(concat "inria-website-html-" suffix) ,(concat "inria-website-static-" suffix)))))

(setq org-publish-project-alist
      (append org-publish-project-alist
      ;; ` character is very important
      ;; replace it by ' and functions inside object will not be executed
      ;; function call have to be prefixed with ,
      `(,@(inria-website-create-project-configuration
           "dev"
           "~/inria/.public_html"
           "")
        ,@(inria-website-create-project-configuration
           "prod"
           "/scp:nfs.saclay.inria.fr:~/public_html/"
           inria-website-url-base)
        ("inria-website-latex"
         :base-directory "~/inria/org/posts"
         :publishing-directory "~/inria/virtual-graphs/notes"
         :publishing-function org-latex-publish-to-latex
         :recursive t))))


