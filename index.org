#+TITLE: Maxime Buron

[[file:img/me.jpg]]

I am a Phd student in [[https://team.inria.fr/cedar/][CEDAR project-team]], supervised by [[http://people.irisa.fr/Francois.Goasdoue/][François Goasdoué]], [[http://pages.saclay.inria.fr/ioana.manolescu/][Ioana Manolescu]] and [[http://www.lirmm.fr/~mugnier/][Marie-Laure Mugnier]]. The subject of my thesis is : Efficient reasoning on heterogeneous large-scale graphs.

*** Email 

firstname.lastname@inria.fr

*** Office

Room 1025 \\
Centre Saclay - Île-de-France \\
Bâtiment Alan Turing 1 rue Honoré d'Estienne d'Orves \\
91120 \\
Palaiseau

*** Software
- [[file:projects/obi-wan/][Obi-Wan]]
*** Publications
- *Revisiting RDF storage layouts for efficient query answering.* Maxime Buron, François Goasdoué, Ioana Manolescu, Tayeb Merabti, Marie-Laure Mugnier. SSWS - 13th International Workshop on Scalable Semantic Web Knowledge Base Systems. [[file:material/ssws20/main.pdf][pdf]]
- *Obi-Wan: Ontology-Based RDF Integration of Heterogeneous Data.* Maxime Buron, François Goasdoué, Ioana Manolescu, Marie-Laure Mugnier. VLDB 2020 - The Forty-sixth International Conference on Very Large Data Bases, Japan. [[https://hal.inria.fr/hal-02921434/document][pdf]] [[file:material/vldb20/video.mp4][video]] [[file:material/vldb20/slides/index.html][slides]]
- *Ontology-Based RDF Integration of Heterogeneous Data.* Maxime Buron, François Goasdoué, Ioana Manolescu, Marie-Laure Mugnier. EDBT/ICDT 2020 - 23rd International Conference on Extending Database Technology, Mar 2020. Copenhagen, Denmark. [[https://hal.inria.fr/hal-02446427/document][pdf]] [[file:material/edbt20/slides.pdf][slides]]
- *Reformulation-based query answering for RDF graphs with RDFS ontologies.* Maxime Buron, François Goasdoué, Ioana Manolescu, Marie-Laure Mugnier. ESWC: European Semantic Web Conference, Jun 2019, Portoroz, Slovenia. [[https://hal.inria.fr/hal-02051413][pdf]]
- *Rewriting-Based Query Answering for Semantic Data Integration Systems.* Maxime Buron, François Goasdoué, Ioana Manolescu, Marie-Laure Mugnier. BDA: Gestion de Données – Principes, Technologies et Applications, Oct 2018, Bucarest, Romania. [[https://hal.inria.fr/hal-01927282][pdf]]
